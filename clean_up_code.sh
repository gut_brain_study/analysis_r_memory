#!/bin/bash
# 
# ## exchange data paths
# sed -i 's|"/data/pt_02020/wd/Behav/memory/model_fits/|paste0(model_fits_dir, "|g' *.R
# 
# sed -i 's|paste0("/data/pt_02020/wd/Behav/memory/|paste0(working_dir, "|g' *.R
# 
# sed -i 's|"/data/pt_02020/wd/Behav/memory/|paste0(working_dir, "|g' *.R
# 
# sed -i 's|"/data/pt_02020/Analysis/analysis_r_memory/Model_comparisons/|paste0(model_comparisons_dir, "|g' *.R
# 
# ## add brackets to close paste0 command
# sed -i 's|.RDS")|.RDS"))|g' *.R
# 
# sed -i 's|.html")|.html"))|g' *.R
# 
# sed -i 's|.csv",|.csv"),|g' *.R
# 
# 
# sed -i 's|.RDS")))|.RDS"))|g' *.R #to correct if run twice
# 
# sed -i 's|.html")))|.html"))|g' *.R #to correct if run twice
# 
# 
# ## insert data paths 
# match='### load dataframe ---------'
# insert='### set paths ------'
# insert1='working_dir = "/data/pt_02020/wd/Behav/memory/"'
# insert2='model_fits_dir = "/data/pt_02020/wd/Behav/memory/model_fits/"'
# insert3='model_comparisons_dir = "/data/pt_02020/Analysis/analysis_r_memory/Model_comparisons/"'
# file='*.R'
# sed -i "s|$match|$insert\n$insert1\n$insert2\n$insert3\n\n$match|" $file
# 
# 
# ## insert data paths 
# match='### load memory performance --------'
# insert='### set paths ------'
# insert1='working_dir = "/data/pt_02020/wd/Behav/memory/"'
# insert2='model_fits_dir = "/data/pt_02020/wd/Behav/memory/model_fits/"'
# insert3='model_comparisons_dir = "/data/pt_02020/Analysis/analysis_r_memory/Model_comparisons/"'
# file='*.R'
# sed -i "s|$match|$insert\n$insert1\n$insert2\n$insert3\n\n$match|" $file
# 
# ## remove library(mice)
# file='*.R'
# sed -i -e '/library(mice)/d' $file
# 
# ## replace loo() with loo_compare()
# match='loo('
# insert='loo_compare('
# file='*.R'
# sed -i "s|$match|$insert|" $file

# ## replace Sex with Gender
# match='Sex'
# insert='Gender'
# file='*.R'
# sed -i "s/$match/$insert/g" $file

# ## replace Reponse correctness with Response accuracy
# match='Response_correctness'
# insert='Response_accuracy'
# file='*.R'
# sed -i "s/$match/$insert/g" $file
# 
# match='Response correctness'
# insert='Response accuracy'
# file='*.R'
# sed -i "s/$match/$insert/g" $file
# 
# match='Response Correctness'
# insert='Response_accuracy'
# file='*.R'
# sed -i "s/$match/$insert/g" $file

# match='title="Response_'
# insert='title="Response '
# file='*.R'
# sed -i "s/$match/$insert/g" $file
# 
# ## replace in model Age + Gender with Gender + Age
# match=' Age + Gender '
# insert=' Gender + Age '
# file='*.R'
# sed -i "s/$match/$insert/g" $file
# 
match='"Age", "Gender (M > F)"'
insert='"Gender (M > F)", "Age"'
file='*.R'
sed -i "s/$match/$insert/g" $file

match='"Age", "Gender (M>F)"'
insert='"Gender (M > F)", "Age"'
file='*.R'
sed -i "s/$match/$insert/g" $file
# 
# ## correct replacement error
# match=':Gender + Age + Age'
# insert=':Gender + Gender + Age'
# file='*.R'
# sed -i "s/$match/$insert/g" $file


