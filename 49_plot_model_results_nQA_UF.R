##### author: thieleking@cbs.mpg.de

### empty environment ------
rm(list=ls())

### load packages -------
library(MetBrewer)
library(sjPlot)
library(ggplot2)
library(brms)
library(magrittr)
library(tidybayes)

### set paths ------
working_dir = "/data/pt_02020/wd/Behav/memory/"
model_fits_dir = "/data/pt_02020/wd/Behav/memory/model_fits/"
model_comparisons_dir = "/data/pt_02020/Analysis/analysis_r_memory/Model_comparisons/"

### load dataframe ---------
recognition_performance <- read.table(paste0(working_dir, "recognition_performance_clean_DWI_whole_brain_anthropometrics.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

### read models ----
full.nQA_UF <- readRDS(paste0(model_fits_dir, "Microstructure/full.nQA_UF.RDS"))
full.nQA_UF.BMI <- readRDS(paste0(model_fits_dir, "Microstructure/full.nQA_UF.BMI.RDS"))
full.nQA_UF.WHR_stand <- readRDS(paste0(model_fits_dir, "Microstructure/full.nQA_UF.WHR_stand.RDS"))
full.nQA_UF.Waist.Height.Ratio_stand <- readRDS(paste0(model_fits_dir, "Microstructure/full.nQA_UF.Waist.Height.Ratio_stand.RDS"))
full.nQA_UF.FM_stand <- readRDS(paste0(model_fits_dir, "Microstructure/full.nQA_UF.FM_stand.RDS"))

null.nQA_UF.noTPinteraction <- readRDS(paste0(model_fits_dir, "Microstructure/null.nQA_UF.noTPinteraction.RDS"))
null.nQA_UF.nogenderinteraction <- readRDS(paste0(model_fits_dir, "Microstructure/null.nQA_UF.nogenderinteraction.RDS"))
null.nQA_UF.BMI.nogenderinteraction <- readRDS(paste0(model_fits_dir, "Microstructure/null.nQA_UF.BMI.nogenderinteraction.RDS"))

### factorize -----
recognition_performance$Timepoint_short <- factor(recognition_performance$Timepoint_short)
recognition_performance$Intervention <- factor(recognition_performance$Intervention)
recognition_performance$Gender <- factor(recognition_performance$Gender)


### plots -----
##### plot settings -----
theme_set(theme_bw(24))

##### set theme for sjPlot --------
set_theme(
  title.size = 2, 
  axis.title.size = 1.7,
  axis.title.color = "black",
  axis.textsize = 1.3,
  axis.textcolor = "grey30", 
  legend.size = 1.3,
  legend.title.size = 1.7,
  geom.label.size = 5,
  base = theme_bw()
)
#####  model results ----------------------------
sjPlot::plot_model(full.nQA_UF, 
                   vline.color = "black", 
                   show.values = TRUE, 
                   value.offset = .3,
                   bpe = "mean",
                   bpe.style = "dot",
                   prob.inner = .5,
                   prob.outer = .95) + sjPlot::theme_sjplot()

ggsave("Figures/Microstructure/UF_full_model_results.png",
       plot = last_plot(),
       device = "png",
       width = 16,
       height = 16,
       units = c("cm"),
       dpi = 300,
       bg = "white") 

sjPlot::plot_model(full.nQA_UF.BMI, 
                   vline.color = "black", 
                   show.values = TRUE, 
                   value.offset = .3,
                   bpe = "mean",
                   bpe.style = "dot",
                   prob.inner = .5,
                   prob.outer = .95) + sjPlot::theme_sjplot()

ggsave("Figures/Microstructure/UF_BMI_full_model_results.png",
       plot = last_plot(),
       device = "png",
       width = 16,
       height = 16,
       units = c("cm"),
       dpi = 300,
       bg = "white") 

sjPlot::plot_model(full.nQA_UF.FM_stand, 
                   vline.color = "black", 
                   show.values = TRUE, 
                   value.offset = .3,
                   bpe = "mean",
                   bpe.style = "dot",
                   prob.inner = .5,
                   prob.outer = .95) + sjPlot::theme_sjplot()

ggsave("Figures/Microstructure/UF_FM_full_model_results.png",
       plot = last_plot(),
       device = "png",
       width = 16,
       height = 16,
       units = c("cm"),
       dpi = 300,
       bg = "white") 

sjPlot::plot_model(full.nQA_UF.Waist.Height.Ratio_stand, 
                   vline.color = "black", 
                   show.values = TRUE, 
                   value.offset = .3,
                   bpe = "mean",
                   bpe.style = "dot",
                   prob.inner = .5,
                   prob.outer = .95) + sjPlot::theme_sjplot()

ggsave("Figures/Microstructure/UF_Waist.Height.Ratio_full_model_results.png",
       plot = last_plot(),
       device = "png",
       width = 16,
       height = 16,
       units = c("cm"),
       dpi = 300,
       bg = "white") 

sjPlot::plot_model(full.nQA_UF.WHR_stand, 
                   vline.color = "black", 
                   show.values = TRUE, 
                   value.offset = .3,
                   bpe = "mean",
                   bpe.style = "dot",
                   prob.inner = .5,
                   prob.outer = .95) + sjPlot::theme_sjplot()

ggsave("Figures/Microstructure/UF_WHR_full_model_results.png",
       plot = last_plot(),
       device = "png",
       width = 16,
       height = 16,
       units = c("cm"),
       dpi = 300,
       bg = "white") 


##### predicted values ----



##### UF by Gender -------
sjPlot::plot_model(full.nQA_UF, type = "pred", terms = c("Gender"), colors = met.brewer("Navajo", n=2)[1:2], axis.lim=c(0.2,0.3), title="Predicted nQA(UF) values depending on gender", axis.title=c("Gender"))

ggsave("Figures/Microstructure/UF_prediction_by_gender.png",
       plot = last_plot(),
       device = "png",
       width = 10,
       height = 10,
       units = c("cm"),
       dpi = 300,
       bg = "white") 


##### UF by age  -------
sjPlot::plot_model(null.nQA_UF.nogenderinteraction, type = "pred", terms = c("Age"), axis.lim=c(0.21,0.33), title="Predicted microstructural coherence of UF by age", axis.title=c("Age","nQA(UF)")) +
  geom_point(aes(Age, nQA_UF_mean, group=Subject, col=Subject, alpha=0.5), recognition_performance, size=3) +
  scale_color_manual(values = met.brewer("Cross",n=59)) +
  theme(legend.position = "none")+
  annotate("text", x=42, y = 0.33,
           label = expression(beta ~ ("Age") ~ " =  0.00 [-0.00, 0.00]"), hjust =1)


ggsave("Figures/Microstructure/UF_prediction_by_age.png",
       plot = last_plot(),
       device = "png",
       width = 20,
       height = 13,
       units = c("cm"),
       dpi = 300,
       bg = "white") 

##### UF by Age and Gender -------
sjPlot::plot_model(full.nQA_UF, type = "pred", terms = c("Age", "Gender"), col = met.brewer("Navajo", n=2)[1:2], axis.lim=c(0.21,0.34), title="Predicted microstructural coherence of UF by age and gender", axis.title=c("Age","nQA(UF)")) +
  geom_point(aes(x=Age, y=nQA_UF_mean, group=Gender, col=Gender), recognition_performance, inherit.aes = F, size=3) +
  annotate("text", x=42, y = 0.34,
           label = expression(beta ~ ("Age * Gender (M>F)") ~ " =  0.00 [-0.00, 0.00]"), hjust =1) +
  annotate("text", x=42, y = 0.33,
         label = expression(beta ~ ("Age") ~ " =  0.00 [-0.00, 0.00]"), hjust =1)

ggsave("Figures/Microstructure/UF_prediction_by_age_and_gender.png",
       plot = last_plot(),
       device = "png",
       width = 23,
       height = 15,
       units = c("cm"),
       dpi = 300,
       bg = "white") 




##### wb by WHR_stand -------
sjPlot::plot_model(full.nQA_UF.WHR_stand, type = "pred", terms = c("WHR_stand"), colors = met.brewer("Navajo", n=2)[1:2], axis.lim=c(0.2,0.35), title="nQA(UF) predicted by Waist-to-Hip-Ratio", axis.title=c("WHR, gender-standardized","nQA(UF)")) +
  geom_point(aes(WHR_stand, nQA_UF_mean, group=Subject, col=Subject, alpha=0.5), recognition_performance) +
  scale_color_manual(values = met.brewer("Cross",n=59)) +
  theme(legend.position = "none") +
  annotate("text", size=6, x=2, y = 0.35,
           label = expression(beta ~ ("WHR") ~ " = 0.00 [-0.01, 0.00]"), hjust =1)


ggsave("Figures/Microstructure/UF_prediction_by_WHR_stand.png",
       plot = last_plot(),
       device = "png",
       width = 20,
       height = 15,
       units = c("cm"),
       dpi = 300,
       bg = "white") 

##### wb by WHR_stand -------
sjPlot::plot_model(full.nQA_UF.FM_stand, type = "pred", terms = c("FM_stand"), colors = met.brewer("Navajo", n=2)[1:2], axis.lim=c(0.2,0.35), title="nQA(UF) predicted by Fat Mass", axis.title=c("FM, gender-standardized","nQA(wb)")) +
  geom_point(aes(FM_stand, nQA_UF_mean, group=Subject, col=Subject, alpha=0.5), recognition_performance) +
  scale_color_manual(values = met.brewer("Cross",n=59)) +
  theme(legend.position = "none") +
  annotate("text", size=6, x=2, y = 0.35,
           label = expression(beta ~ ("FM") ~ " = 0.00 [-0.01, 0.01]"), hjust =1)


ggsave("Figures/Microstructure/UF_prediction_by_FM_stand.png",
       plot = last_plot(),
       device = "png",
       width = 20,
       height = 15,
       units = c("cm"),
       dpi = 300,
       bg = "white") 



### data simulation ----------------------
### plotting posterior draws with CI interval -----
get_variables(full.nQA_UF)[1:20]

preds <- recognition_performance %>%
  add_predicted_draws(full.nQA_UF) 

ggplot(preds) + 
  stat_pointinterval(aes(x = .row, y = .prediction)) +
  geom_point(aes(x = .row, y = nQA_UF_mean), color = "red")


### add predicted nQA values to original data --------
pp <- posterior_predict(null.nQA_UF.nogenderinteraction, recognition_performance, allow_new_levels=T, ndraws=5000)

for(i in 1:length(recognition_performance$Subject)){
  recognition_performance$nQA_UF_pred[i] <- mean(pp[,i])
}

### plot predicted values --------
ggplot(recognition_performance, aes(Age, nQA_UF_pred)) +
  geom_point(aes(y=nQA_UF_mean), colour="red", alpha=0.5) +
  geom_point(alpha=.5) +
  labs(title="UF over Age")+
  xlab("Age") +
  ylab("predicted nQA(UF)")


### simulate newdata -----------
newdata <- data.frame(Subject=rep(1:100,each=4))
seed=123
newdata$Timepoint_short <- c("BL", "FU")
newdata$Intervention <- rep(sample(c("A", "B"), length(newdata$Subject)/2, replace=T), each=2)
newdata$Age <- sample(18:45, length(newdata$Subject), replace=T)
newdata$Gender <- sample(c("F", "M"), length(newdata$Subject), replace=T)
newdata$BMI <- sample(seq(25,30,0.1), length(newdata$Subject), replace=T)
newdata$WHR_stand <- sample(seq(0.68,0.98,0.01), length(newdata$Subject), replace=T)
newdata$Waist.Height.Ratio_stand <- sample(seq(0.43,0.58,0.01), length(newdata$Subject), replace=T)
newdata$FM_stand <- sample(seq(-3.53,2.44,0.01), length(newdata$Subject), replace=T)

pp <- posterior_predict(full.nQA_UF, newdata, allow_new_levels=T, ndraws=5000)

for(i in 1:length(newdata$Subject)){
  newdata$nQA_UF_pred[i] <- mean(pp[,i])
}

### plot predicted simulated values (errors somewhere)--------
ggplot(newdata, aes(Age, nQA_UF_pred)) +
  geom_point() +
  #geom_smooth(method="lm")+
  labs(title="UF over Age")+
  xlab("Age") +
  ylab("predicted nQA(UF)")

ggplot(newdata, aes(Age, nQA_UF_pred)) +
  geom_point(aes(group=Gender, col=Gender)) +
  scale_color_manual(values = met.brewer("Navajo",n=2)) +
  labs(title="UF over Age by Gender")+
  xlab("Age") +
  ylab("predicted nQA(UF)")

ggplot(newdata, aes(WHR_stand, nQA_UF_pred)) +
  geom_point(aes(group=Gender, col=Gender)) +
  scale_color_manual(values = met.brewer("Navajo",n=2)) +
  labs(title="UF over WHR_stand by Gender")+
  xlab("WHR_stand") +
  ylab("predicted nQA(UF)")

ggplot(newdata, aes(BMI, nQA_UF_pred)) +
  geom_point(aes(group=Gender, col=Gender)) +
  scale_color_manual(values = met.brewer("Navajo",n=2)) +
  labs(title="UF over BMI by Gender")+
  xlab("BMI") +
  ylab("predicted nQA(UF)")

