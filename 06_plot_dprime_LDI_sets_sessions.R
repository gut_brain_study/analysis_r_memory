##### author: thieleking@cbs.mpg.de

#remove all dataframes in environment
rm(list=ls())

### load packages ------
library(
ggplot2)
library(MetBrewer)
library(vioplot)

### set paths ------
working_dir = "/data/pt_02020/wd/Behav/memory/"


#### load recognition & discrimination performance --------
recognition_performance <- read.table(paste0(working_dir, "recognition_performance.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)
recognition_performance_F_NF <- read.table(paste0(working_dir, "recognition_performance_F_NF.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)
recognition_performance_F <- read.table(paste0(working_dir, "recognition_performance_F.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)
recognition_performance_NF <- read.table(paste0(working_dir, "recognition_performance_NF.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

discrimination_performance <- read.table(paste0(working_dir, "discrimination_performance.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)
discrimination_performance_F_NF <- read.table(paste0(working_dir, "discrimination_performance_F_NF.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)
discrimination_performance_F <- read.table(paste0(working_dir, "discrimination_performance_F.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)
discrimination_performance_NF <- read.table(paste0(working_dir, "discrimination_performance_NF.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

#### plot d' value distribution --------------
cairo_ps("Figures/dprime_distribution.eps",
         width = 10,
         height = 6,
         bg = "white")

ggplot(recognition_performance, aes(x = dprime)) +
  geom_histogram(aes(y=..density..), binwidth=.1, fill="white", color="black", alpha=0.4) +
  geom_density(color = "darkturquoise", lwd = 0.9, alpha = 0.4) +
  xlim(-0.3,3.1) +
  ggtitle("Shapiro-Wilk normality test: W = 0.9864, p-value = 0.05291
          --> d' is normally distributed")
dev.off()

#### plot LDI value distribution --------------

cairo_ps("Figures/LDI_distribution.eps",
         width = 10,
         height = 6,
         bg = "white")

ggplot(discrimination_performance, aes(x = LDI)) +
  geom_histogram(aes(y=..density..), binwidth=.1, fill="white", color="black", alpha=0.4) +
  geom_density(color = "cornflowerblue", lwd = 1.1, alpha = 0.4) +
  xlim(-0.3,3.1) +
  ggtitle("Shapiro-Wilk normality test: W = 0.99262, p-value = 0.4167
          --> LDI is normally distributed")
dev.off()

#### plot d' value distribution by set --------------

cairo_ps("Figures/Set/dprimeoverSet.eps",
         width = 10,
         height = 6,
         bg = "white")

ggplot(recognition_performance, aes(x = dprime, fill=Set)) +
  geom_histogram(aes(y=..density..), binwidth=.1) +
  scale_fill_manual(values=met.brewer("Cross", n=4)) +
  facet_grid(Set ~ .) +
  geom_density(color = "darkturquoise", lwd = 0.9, alpha = 0.4) +
  xlim(-0.3,3.1)
dev.off()

#### plot d' value distribution by set with separate categories--------------

cairo_ps("Figures/Set/dprimeoverSet_F_NF.eps",
         width = 10,
         height = 6,
         bg = "white")

ggplot(recognition_performance_F_NF, aes(x = dprime, fill=Set)) +
  geom_histogram(aes(y=..density..), binwidth=.1) +
  scale_fill_manual(values=met.brewer("Cross", n=4)) +
  facet_grid(Set ~ Category) +
  geom_density(color = "darkturquoise", lwd = 0.9, alpha = 0.4) +
  xlim(-0.6,5.3)

dev.off()


#### plot LDI value distribution by set--------------

cairo_ps("Figures/Set/LDIoverSet.eps",
         width = 10,
         height = 6,
         bg = "white")

ggplot(discrimination_performance, aes(x = LDI, fill=Set)) +
  geom_histogram(aes(y=..density..), binwidth=.1) +
  scale_fill_manual(values=met.brewer("Cross", n=4)) +
  facet_grid(Set ~ .) +
  geom_density(color = "cornflowerblue", lwd = 0.9, alpha = 0.4) +
  xlim(-0.3,3.1)

dev.off()

#### plot LDI value distribution by set with separate categories--------------

cairo_ps("Figures/Set/LDIoverSet_F_NF.eps",
         width = 10,
         height = 6,
         bg = "white")

ggplot(discrimination_performance_F_NF, aes(x = LDI, fill=Set)) +
  geom_histogram(aes(y=..density..), binwidth=.1) +
  scale_fill_manual(values=met.brewer("Cross", n=4)) +
  facet_grid(Set ~ Category) +
  geom_density(color = "cornflowerblue", lwd = 0.9, alpha = 0.4) +
  xlim(-0.6,5.3)

dev.off()




#### plot d' value distribution by session ----- 
recognition_performance$Session = as.factor(recognition_performance$Session)

cairo_ps("Figures/Session/dprimeoverSession.eps",
         width = 10,
         height = 6,
         bg = "white")

ggplot(recognition_performance, aes(x = dprime, fill=Session)) +
  geom_histogram(aes(y=..density..), binwidth=.1) +
  scale_fill_manual(values=met.brewer("Morgenstern", n=4)) +
  facet_grid(Session ~ .) +
  geom_density(color = "darkturquoise", lwd = 0.9, alpha = 0.4) +
  xlim(-0.3,3.1)

dev.off()

#### plot d' value distribution by session with separate categories--------------
recognition_performance_F_NF$Session = as.factor(recognition_performance_F_NF$Session)

cairo_ps("Figures/Session/dprimeoverSession_F_NF.eps",
         width = 10,
         height = 6,
         bg = "white")

ggplot(recognition_performance_F_NF, aes(x = dprime, fill=Session)) +
  geom_histogram(aes(y=..density..), binwidth=.1) +
  scale_fill_manual(values=met.brewer("Morgenstern", n=4)) +
  facet_grid(Session ~ Category) +
  geom_density(color = "darkturquoise", lwd = 0.9, alpha = 0.4) +
  xlim(-0.6,5.3)

dev.off()



#### plot LDI value distribution by session ----- 
discrimination_performance$Session = as.factor(discrimination_performance$Session)

cairo_ps("Figures/Session/LDIoverSession.eps",
         width = 10,
         height = 6,
         bg = "white")

ggplot(discrimination_performance, aes(x = LDI, fill=Session)) +
  geom_histogram(aes(y=..density..), binwidth=.1) +
  scale_fill_manual(values=met.brewer("Morgenstern", n=4)) +
  facet_grid(Session ~ .) +
  geom_density(color = "cornflowerblue", lwd = 0.9, alpha = 0.4) +
  xlim(-0.3,3.1)

dev.off()

#### plot LDI value distribution by session with separate categories--------------
discrimination_performance_F_NF$Session = as.factor(discrimination_performance_F_NF$Session)

cairo_ps("Figures/Session/LDIoverSession_F_NF.eps",
         width = 10,
         height = 6,
         bg = "white")

ggplot(discrimination_performance_F_NF, aes(x = LDI, fill=Session)) +
  geom_histogram(aes(y=..density..), binwidth=.1) +
  scale_fill_manual(values=met.brewer("Morgenstern", n=4)) +
  facet_grid(Session ~ Category) +
  geom_density(color = "cornflowerblue", lwd = 0.9, alpha = 0.4) +
  xlim(-0.6,5.3)

dev.off()



### violin plots dprime over Sets with single subjects ------

cairo_ps("Figures/Set/dprimeoverSets_single_subjects.eps",
         width = 10,
         height = 6,
         bg = "white")

ggplot(recognition_performance, aes(Set, dprime)) +
  geom_point(aes(group=Subject, col=Subject, alpha=0.5), position = position_dodge(0.4)) +
  geom_line(aes(group=Subject, col=Subject, alpha=0.5), position = position_dodge(0.4)) +
  geom_violin(alpha=0.5, fill="darkgrey") +
  geom_boxplot(width=0.2, alpha=0.5, fill=met.brewer("Hokusai3", n=4)) +
  theme(legend.position = "none") +
  scale_color_manual(values = met.brewer("Cross", n=64)) +
  labs(title="Distribution of d' values over picture sets", subtitle="with single subjects")

dev.off()

### violin plots dprime over Sets and F & NF with single subjects ------
cairo_ps("Figures/Set/dprimeoverSets_F_NF_single_subjects.eps",
         width = 10,
         height = 6,
         bg = "white")

ggplot(recognition_performance_F_NF, aes(Set, dprime)) +
  geom_point(aes(group=Subject, col=Subject, alpha=0.5), position = position_dodge(0.4)) +
  geom_line(aes(group=Subject, col=Subject, alpha=0.5), position = position_dodge(0.4)) +
  geom_violin(alpha=0.5, fill="darkgrey") +
  geom_boxplot(width=0.2, alpha=0.5, fill=rep(met.brewer("Hokusai3", n=4)[1:4],2)) +
  theme(legend.position = "none") +
  scale_color_manual(values = met.brewer("Cross", n=60)) +
  facet_grid(. ~ Category) +
  labs(title="Distribution of d' values over picture sets", subtitle="with single subjects")
dev.off()


### violin plots LDI over Sets with single subjects ------
cairo_ps("Figures/Set/LDIoverSets_single_subjects.eps",
         width = 10,
         height = 6,
         bg = "white")

ggplot(discrimination_performance, aes(Set, LDI )) +
  geom_point(aes(group=Subject, col=Subject, alpha=0.5), position = position_dodge(0.4)) +
  geom_line(aes(group=Subject, col=Subject, alpha=0.5), position = position_dodge(0.4)) +
  geom_violin(alpha=0.5, fill="darkgrey") +
  geom_boxplot(width=0.2, alpha=0.5, fill=met.brewer("Hokusai3", n=4)) +
  scale_color_manual(values = met.brewer("Cross", n=64)) +
  theme(legend.position = "none") +
  labs(title="Distribution of LDI values over picture sets", subtitle="with single subjects")

dev.off()

### violin plots LDI over Sets and F & NF with single subjects ------
cairo_ps("Figures/Set/LDIoverSets_F_NF_single_subjects.eps",
         width = 10,
         height = 6,
         bg = "white")

ggplot(discrimination_performance_F_NF, aes(Set, LDI )) +
  geom_point(aes(group=Subject, col=Subject, alpha=0.5), position = position_dodge(0.4)) +
  geom_line(aes(group=Subject, col=Subject, alpha=0.5), position = position_dodge(0.4)) +
  geom_violin(alpha=0.5, fill="darkgrey") +
  geom_boxplot(width=0.2, alpha=0.5, fill=rep(met.brewer("Hokusai3", n=4)[1:4],2)) +
  scale_color_manual(values = met.brewer("Cross", n=64)) +
  theme(legend.position = "none") +
  facet_grid(. ~ Category) +
  labs(title="Distribution of LDI values over picture sets", subtitle="with single subjects")

dev.off()

### violin plots dprime over Sessions with single subjects ------
cairo_ps("Figures/Session/dprimeoverSessions_single_subjects.eps",
         width = 10,
         height = 6,
         bg = "white")

ggplot(recognition_performance, aes(Session, dprime )) +
  geom_point(aes(group=Subject, col=Subject, alpha=0.5), position = position_dodge(0.4)) +
  geom_line(aes(group=Subject, col=Subject, alpha=0.5), position = position_dodge(0.4)) +
  geom_violin(alpha=0.5, fill="darkgrey") +
  geom_boxplot(width=0.2, alpha=0.5, fill=met.brewer("Greek", n=4)) +
  theme(legend.position = "none") +
  scale_color_manual(values = met.brewer("Cross", n=64)) +
  labs(title="Distribution of dprime values over sessions", subtitle="with single subjects")

dev.off()
### violin plots dprime over Sessions and F & NF with single subjects ------
cairo_ps("Figures/Session/dprimeoverSessions_F_NF_single_subjects.eps",
         width = 10,
         height = 6,
         bg = "white")

ggplot(recognition_performance_F_NF, aes(Session, dprime )) +
  geom_point(aes(group=Subject, col=Subject, alpha=0.5), position = position_dodge(0.4)) +
  geom_line(aes(group=Subject, col=Subject, alpha=0.5), position = position_dodge(0.4)) +
  geom_violin(alpha=0.5, fill="darkgrey") +
  geom_boxplot(width=0.2, alpha=0.5, fill=rep(met.brewer("Greek", n=4)[1:4],2)) +
  theme(legend.position = "none") +
  scale_color_manual(values = met.brewer("Cross", n=64)) +
  facet_grid(. ~ Category) +
  labs(title="Distribution of dprime values over sessions", subtitle="with single subjects")

dev.off()

### violin plots LDI over Sessions with single subjects ------
cairo_ps("Figures/Session/LDIoverSessions_single_subjects.eps",
         width = 10,
         height = 6,
         bg = "white")

ggplot(discrimination_performance, aes(Session, LDI)) +
  geom_point(aes(group=Subject, col=Subject, alpha=0.5), position = position_dodge(0.4)) +
  geom_line(aes(group=Subject, col=Subject, alpha=0.5), position = position_dodge(0.4)) +
  geom_violin(alpha=0.5, fill="darkgrey") +
  geom_boxplot(width=0.2, alpha=0.5, fill=met.brewer("Greek", n=4)) +
  theme(legend.position = "none") +
  scale_color_manual(values = met.brewer("Cross", n=64)) +
  labs(title="Distribution of LDI values over sessions", subtitle="with single subjects")

dev.off()

### violin plots LDI over Sessions and F & NF with single subjects ------
cairo_ps("Figures/Session/LDIoverSessions_F_NF_single_subjects.eps",
         width = 10,
         height = 6,
         bg = "white")

ggplot(discrimination_performance_F_NF, aes(Session, LDI)) +
  geom_point(aes(group=Subject, col=Subject, alpha=0.5), position = position_dodge(0.4)) +
  geom_line(aes(group=Subject, col=Subject, alpha=0.5), position = position_dodge(0.4)) +
  geom_violin(alpha=0.5, fill="darkgrey") +
  geom_boxplot(width=0.2, alpha=0.5, fill=rep(met.brewer("Greek", n=4)[1:4],2)) +
  theme(legend.position = "none") +
  scale_color_manual(values = met.brewer("Cross", n=64)) +
  facet_grid(. ~ Category) +
  labs(title="Distribution of LDI values over sessions", subtitle="with single subjects")

dev.off()
