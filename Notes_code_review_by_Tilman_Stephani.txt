##### Code review Memory task in Gut-Brain study #####
# Tilman Stephani 09/2022


### Versions/ dependencies:
- It was not possible to install brms version 2.16.3 under R version 4.1.2 on institute server. Used R version 4.2.0 with brms version 2.17.0 instead.
- UPDATE: After updating package "stringi", brms version 2.16.3 worked under R version 4.1.2. From script 15.1 on, this environment was used in this code review (incl. "loo" version 2.4.1).


### Script 01
- lines 60 ff.: Output indicated 107 images for S06 FU1 (instead of 106 according to comment in script)

- lines 102-106: After running these lines, I noticed that there were still entries in memory_task_corrected$Code that simply say "missed". Was this intended (since all the other categories code F vs NF responses)? -> After going through script 04 I guess this was intended.


### Script 02
- General suggestion (also relating to the other scripts): It may simplify the code (and facilitate re-running the analysis for others) if the path name of the data is pre-allocated and specified only once at the beginning of the script because it stays constant throughout all scripts, e.g.:
data_path <- '/data/pt_01972/Code_review/Gut_Brain/' 
performance_total <- read.table(paste0(data_path, "memory_performance_total.csv"), ... )
This especially also applies to save commands later in the scripts.


### Script 03
- I noticed that I hadn´t installed all the packages; it may help to list all required packages in the readme of the repository so that the user gets an overview already at some central place what packages need to be installed..?

- lines 18-70: I assume this should be uncommented, right? (I now ran the script by simply loading the data in line 71 though.)
-> REPLY: Done.

- lines 76-84: I got 44 warning messages "number of items to replace is not a multiple of replacement length". This was due to line 82 trying to assign two (or more?) wanting values to the left side of the assignment. I hope this only applies to subject S01 which is anyways removed in script 05 ...?
-> REPLY: Yes, as the pilot subject S01 had different picture sets from the rest


- lines 91-134: Should be uncommented? (I ran the script with the import command in line 137.)

- I saved the histogram at the end for comparison here (to double-check that it looks like intended): https://owncloud.gwdg.de/index.php/s/0WtXGaitlO2VT77


### Script 04
- ran through smoothly


### Script 05
- ran through smoothly (as discussed I did not check in detail whether dprime and LDI equations are correct)
- my only suggestion would be to define the data path only once and assign it to a variable (see my related comment above)
-> REPLY: Done.


### Script 09
- lines 24-30: elegant code :-) maybe omit the commented lines below ("version 2")?
-> REPLY: Done.

- line 79: The unequal numbers of trials in the wanting categories (tertiles) point to quite skewed distributions of wanting values -> one could try to log-transform these values to obtain more suitable distributions for later statistics (maybe too late now)
-> REPLY: Problem: food pictures are generally higher rated than non-food. therefore not transformed but exclusion is logically okay.

- lines 136-139: Aren´t the "for" and "if" statements unnecessary (you only look at the image and wanting category of the current row of "recognition_performance_F_NF_wanting" anyways)? Or maybe I didn´t understand something here? (same in lines 156-174) 
-> REPLY: is necessary as some wanting categories are empty


### Script 10
- did not double-check the specific equations!


### Script 11
- lines 34-39: looked a bit complicated to overwrite the dataframes in every loop iteration ;-) but I guess it worked
(something like "xy_clean <- xy[-which(xy$Subject==subj_excl & xy$Session==session_excl),]" could provide a more efficient solution; but please double-check!)


### Script 13
- packages "rstatix" and "ggpubr" need to be installed before
-> REPLY: added to README

### Script 14
- typo in name of script: "collinearity" ;-)
-> REPLY: corrected

- packages "GGally", "ggExtra", and "mctest" need to be installed before
-> REPLY: corrected which packages have to be loaded and added to README

- lines 21-34: all these datasets were not created in the scripts before - maybe add a note where these datasets are from (or change arrangements of scripts)? ---> continued with files from "/data/pt_02020/wd/Behav/memory/" but "recognition_performance_clean_with_fev_edeq.csv" not available here as well
-> REPLY: left a note and changed the filename "recognition_performance_clean_with_fev_edeq.csv" to "recognition_performance_clean_with_tfeq_edeq.csv"

- did not check all the plotting code

- line 192: strictly speaking, lm() (and cor() in line 218) do not account for the nested structure of the data (there are several dprime and LDI values per participant = repeated measurements) so the related inference statistics may not be 100% accurate. But I assume this is not a central analysis in your study, right..? 
-> REPLY: Correct, the outcome of these plots is not statistically evaluated. I used it to get an impression of covariates for further statistical analysis.


------ from here only checking of some arbitrarily chosen scripts:

### Script 15.1
- dependencies: mice, sjPlot
-> REPLY: added to README

- lines 37-193: maybe omit commented lines if not needed?
-> REPLY: clarified the commented lines in all scripts

(- for being able to run brm(), package 'stringi' needed to be updated)

- line 201: "list(adapt_delta = 0.995)" instead of "list(adapt_delta = 0.99)" according to "Analysis Plan" on OSF (not so crucial I guess)
-> REPLY: in a few cases this number had to be increased in order for the model fit to be accurate. Further explanation is in the preregistration.

### Script 17.1

- question: Why do you sometimes use loo() and sometimes compare_loo() if you add loo to all models anyways? (maybe this could be simplified; but I am not experienced with this, so it might be irrelevant)
-> REPLY: loo_compare() gives a more condensed output. Changed all loo() to loo_compare().

### Script 30
- no further comments (but only skimmed script)


### Script 36
- no further comments (but only skimmed script)


### Script 39
- no further comments (but only skimmed script)


### Script 57
- lines 38-40: If I am not mistaken all serum_marker values "<XY" (e.g. S02, BL1, STPYY: <13.7) cause NAs in recognition_performance -> Is this intended or should these values rather be 0 ("below detection threshold" if I interpret it correctly)? ...same in lines 45-54 and 56-65
-> REPLY: No, NA is correct as these values were too low to be detected and therefore should not enter the statistical analysis

- lines 68-78: I couldn´t find any "X" entries in all these dataframes -> as.numeric("X") = NA (as done in code above)
-> REPLY: correct. X's were transformed earlier into NAs

### Script 58
- line 27: no variable "Set" in memory_task as loaded with line 19 (from pt_02020)
-> REPLY: Correct, memory_task includes single images and should only contain the variable "Image" for random effect modelling. However, could not be run, as too little compute power for single image analysis

- question: how does brm handle the NAs in the serum level variables? (maybe make this more explicit somewhere if it is a default option?)
-> REPLY: NAs were not imputed but complete data point excluded from analysis.
-> Variables with NAs: z_Arousal (n=484 from 11 images), VAS_anxiety + VAS_nausea (n=1), SES (n=14), serum_GHRL (n=36), serum_GLP1 (n=36), serum_PYY (n=300)
-> serum_GLP1 and serum_PYY not of interest
-> imputation with package mice should be done in an additional analysis for Arousal, VAS, SES and ghrelin

(- I did not look at the plotting code at the end of this script)



















