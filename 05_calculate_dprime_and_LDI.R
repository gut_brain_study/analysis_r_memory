##### author: thieleking@cbs.mpg.de

#remove all dataframes in environment
rm(list=ls())

### load packages ------
library(stringr)
library(tibble)
library(magrittr)

### set paths ------
working_dir = "/data/pt_02020/wd/Behav/memory/"

#### load memory performance --------
recognition_performance_F_NF <- read.table(paste0(working_dir, "memory_performance_F_NF.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

recognition_performance <- read.table(paste0(working_dir, "memory_performance_total.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

lures <- read.table(paste0(working_dir, "memory_performance_lures.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

novels <- read.table(paste0(working_dir, "memory_performance_novels.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

targets <- read.table(paste0(working_dir, "memory_performance_targets.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

lures_F_NF <- read.table(paste0(working_dir, "memory_performance_lures_F_NF.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

novels_F_NF <- read.table(paste0(working_dir, "memory_performance_novels_F_NF.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

targets_F_NF <- read.table(paste0(working_dir, "memory_performance_targets_F_NF.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)


## exclude subject S01 due to piloting ----------------------

recognition_performance <- recognition_performance[!recognition_performance$Subject == "S01",]
lures <- lures[!lures$Subject == "S01",]
novels <- novels[!novels$Subject == "S01",]
targets <- targets[!targets$Subject == "S01",]

recognition_performance_F_NF <- recognition_performance_F_NF[!recognition_performance_F_NF$Subject == "S01",]
lures_F_NF <- lures_F_NF[!lures_F_NF$Subject == "S01",]
novels_F_NF <- novels_F_NF[!novels_F_NF$Subject == "S01",]
targets_F_NF <- targets_F_NF[!targets_F_NF$Subject == "S01",]

### create discrimination performance dataframes --------
discrimination_performance = recognition_performance[,1:12]
discrimination_performance_F_NF = recognition_performance_F_NF[,1:13]

discrimination_performance$n_corr_rej_lures = lures$n_corr_rej
discrimination_performance$n_miss_targets = targets$n_miss
discrimination_performance$sum_lures = recognition_performance$sum_lures
discrimination_performance$sum_targets = recognition_performance$sum_targets

discrimination_performance_F_NF$n_corr_rej_lures = lures_F_NF$n_corr_rej
discrimination_performance_F_NF$n_miss_targets = targets_F_NF$n_miss
discrimination_performance_F_NF$sum_lures = lures_F_NF$sum
discrimination_performance_F_NF$sum_targets = targets_F_NF$sum

#### quality check before d' calculation: n_hit = sum_target or n_false_alarm = 0--------
sum(recognition_performance$n_hit == recognition_performance$sum_targets)
sum(recognition_performance_F_NF$n_hit == recognition_performance_F_NF$sum_targets)

sum(recognition_performance$n_false_alarm == 0)
sum(recognition_performance_F_NF$n_false_alarm == 0)

#### quality check before LDI calculation: n_corr_rej_lures = sum_lures or n_miss_targets = 0--------
sum(discrimination_performance$n_corr_rej_lures == discrimination_performance$sum_lures)
sum(discrimination_performance_F_NF$n_corr_rej_lures == discrimination_performance_F_NF$sum_lures)

sum(discrimination_performance$n_miss_targets == 0)
sum(discrimination_performance_F_NF$n_miss_targets == 0)


# calculate for extreme rates corrected d' ----------

##### dprime = qnorm(n_hit+0.5/sum_targets+1) - qnorm(n_false_alarms+0.5/sum_lures_novels+1)
### calc dprime
recognition_performance$dprime <- qnorm((recognition_performance$n_hit+0.5)/(recognition_performance$sum_targets+1)) - qnorm((recognition_performance$n_false_alarm+0.5)/(recognition_performance$sum_lures+recognition_performance$sum_novels+1))
                                                                                                                
recognition_performance_F_NF$dprime <- qnorm((recognition_performance_F_NF$n_hit+0.5)/(recognition_performance_F_NF$sum_targets+1)) - qnorm((recognition_performance_F_NF$n_false_alarm+0.5)/(recognition_performance_F_NF$sum_lures+recognition_performance_F_NF$sum_novels+1))
                                                                                                        
# calculate for extreme rates corrected LDI ----------

# LDI = qnorm(n_corr_rej_lures+1/3/sum_lures+2/3) - qnorm(n_miss_targets+0.5/sum_targets+1)

discrimination_performance$LDI = qnorm((discrimination_performance$n_corr_rej_lures+round(1/3, digits=2))/(discrimination_performance$sum_lures+round(2/3, digits=2))) - qnorm((discrimination_performance$n_miss_targets+0.5)/(discrimination_performance$sum_targets+1))

discrimination_performance_F_NF$LDI = qnorm((discrimination_performance_F_NF$n_corr_rej_lures+round(1/3, digits=2))/(discrimination_performance_F_NF$sum_lures+round(2/3, digits=2))) - qnorm((discrimination_performance_F_NF$n_miss_targets+0.5)/(discrimination_performance_F_NF$sum_targets+1))

### Leal et al 2014 only probabilities
discrimination_performance$LDI_prob = (discrimination_performance$n_corr_rej_lures/discrimination_performance$sum_lures) - (discrimination_performance$n_miss_targets/discrimination_performance$sum_targets)

### create datasets for F and NF separately ------
recognition_performance_F = recognition_performance_F_NF[recognition_performance_F_NF$Category == "F",]
recognition_performance_NF = recognition_performance_F_NF[recognition_performance_F_NF$Category == "NF",]
discrimination_performance_F = discrimination_performance_F_NF[discrimination_performance_F_NF$Category == "F",]
discrimination_performance_NF = discrimination_performance_F_NF[discrimination_performance_F_NF$Category == "NF",]



#### export dataframes --------

write.table(recognition_performance, paste0(working_dir, "recognition_performance.csv"), quote = FALSE, sep = "\t", row.names = F)
write.table(recognition_performance_F_NF, paste0(working_dir, "recognition_performance_F_NF.csv"), quote = FALSE, sep = "\t", row.names = F)
write.table(recognition_performance_F, paste0(working_dir, "recognition_performance_F.csv"), quote = FALSE, sep = "\t", row.names = F)
write.table(recognition_performance_NF, paste0(working_dir, "recognition_performance_NF.csv"), quote = FALSE, sep = "\t", row.names = F)
write.table(discrimination_performance, paste0(working_dir, "discrimination_performance.csv"), quote = FALSE, sep = "\t", row.names = F)
write.table(discrimination_performance_F_NF, paste0(working_dir, "discrimination_performance_F_NF.csv"), quote = FALSE, sep = "\t", row.names = F)
write.table(discrimination_performance_F, paste0(working_dir, "discrimination_performance_F.csv"), quote = FALSE, sep = "\t", row.names = F)
write.table(discrimination_performance_NF, paste0(working_dir, "discrimination_performance_NF.csv"), quote = FALSE, sep = "\t", row.names = F)

### check for normality ---------
shapiro.test(recognition_performance$dprime)
shapiro.test(discrimination_performance$LDI)

##### d' and LDI normally distributed -----

### check correlation ----
cor(recognition_performance$dprime, discrimination_performance$LDI, method = "pearson")
