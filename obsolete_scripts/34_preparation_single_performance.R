##### author: thieleking@cbs.mpg.de

#remove all dataframes in environment
rm(list=ls())

### load packages -----
library(tibble)

### set paths ------
working_dir = "/data/pt_02020/wd/Behav/memory/"
model_fits_dir = "/data/pt_02020/wd/Behav/memory/model_fits/"
model_comparisons_dir = "/data/pt_02020/Analysis/analysis_r_memory/Model_comparisons/"


### load memory performance --------

memory_task <- readRDS(paste0(working_dir, "memory_task_output_correctedSDT_with_wanting_and_image_characteristica.RDS"))
memory_task_liking <- readRDS(paste0(working_dir, "memory_task_output_correctedSDT_with_wanting_and_liking_and_image_characteristica.RDS"))

recognition_performance <- read.table(paste0(working_dir, "recognition_performance.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

### prepare dataframe -------

memory_task_complete$Session <- factor(memory_task_complete$Session)
memory_task_complete$Category <- factor(memory_task_complete$Category, levels=c("NF", "F"))
memory_task_complete$Status <- factor(memory_task_complete$Status)
memory_task_complete$Type <- factor(memory_task_complete$Type)
memory_task_complete$Image_Number <- factor(memory_task_complete$Image_Number)
memory_task_complete$Style <- factor(memory_task_complete$Style)
memory_task_complete$Code <- factor(memory_task_complete$Code)
memory_task_complete$Wanting <- as.numeric(memory_task_complete$Wanting)
memory_task_complete$response_correct <- factor(memory_task_complete$response_correct, levels=c(0,1), labels=c("incorrect", "correct"))

memory_task_liking$Session <- factor(memory_task_liking$Session)
memory_task_liking$Category <- factor(memory_task_liking$Category, levels=c("NF", "F"))
memory_task_liking$Status <- factor(memory_task_liking$Status)
memory_task_liking$Type <- factor(memory_task_liking$Type)
memory_task_liking$Image_Number <- factor(memory_task_liking$Image_Number)
memory_task_liking$Style <- factor(memory_task_liking$Style)
memory_task_liking$Code <- factor(memory_task_liking$Code)
memory_task_liking$Wanting <- as.numeric(memory_task_liking$Wanting)
memory_task_liking$response_correct <- factor(memory_task_liking$response_correct, levels=c(0,1), labels=c("incorrect", "correct"))

### remove subjects excluded from behavioural analysis ------
excluded_subjects <- read.table(paste0(working_dir, "excluded_subjects_from_memory.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

for(subj in excluded_subjects$Subject){
  sessions=excluded_subjects$Session[excluded_subjects$Subject == subj]
  for(ses in sessions){
    memory_task_complete <- memory_task_complete[!(memory_task_complete$Subject==subj & memory_task_complete$Session == ses),]
  }
}

memory_task_liking <- memory_task_liking[!(memory_task_liking$Subject=="S01"),]
for(subj in excluded_subjects$Subject){
  tps=excluded_subjects$Timepoint[excluded_subjects$Subject == subj]
  for(tp in tps){
    memory_task_liking <- memory_task_liking[!(memory_task_liking$Subject==subj & memory_task_liking$Timepoint == tp),]
  }
}


### add columns ---------
memory_task_complete <- add_column(memory_task_complete, Timepoint_short=rep(NA, length(memory_task_complete$Subject)), .after="Timepoint")
memory_task_complete <- add_column(memory_task_complete, Age=rep(NA, length(memory_task_complete$Subject)), .before="Image")
memory_task_complete <- add_column(memory_task_complete, Gender=rep(NA, length(memory_task_complete$Subject)), .before="Image")
memory_task_complete <- add_column(memory_task_complete, Intervention=rep(NA, length(memory_task_complete$Subject)), .before="Image")

memory_task_liking <- add_column(memory_task_liking, Timepoint_short=rep(NA, length(memory_task_liking$Subject)), .after="Timepoint")
memory_task_liking <- add_column(memory_task_liking, Age=rep(NA, length(memory_task_liking$Subject)), .before="Image")
memory_task_liking <- add_column(memory_task_liking, Gender=rep(NA, length(memory_task_liking$Subject)), .before="Image")
memory_task_liking <- add_column(memory_task_liking, Intervention=rep(NA, length(memory_task_liking$Subject)), .before="Image")

memory_task_complete$Timepoint_short[memory_task_complete$Timepoint == "BL1" | memory_task_complete$Timepoint == "BL2"] <- "BL"
memory_task_complete$Timepoint_short[memory_task_complete$Timepoint == "FU1" | memory_task_complete$Timepoint == "FU2"] <- "FU"

memory_task_liking$Timepoint_short[memory_task_liking$Timepoint == "BL1" | memory_task_liking$Timepoint == "BL2"] <- "BL"
memory_task_liking$Timepoint_short[memory_task_liking$Timepoint == "FU1" | memory_task_liking$Timepoint == "FU2"] <- "FU"

for(i in 1:length(memory_task_complete$Subject)){
  subj = memory_task_complete$Subject[i]
  tp = memory_task_complete$Timepoint[i]
  memory_task_complete$Age[i] = recognition_performance$Age[recognition_performance$Subject == subj & recognition_performance$Timepoint == tp]
  memory_task_complete$Gender[i] = recognition_performance$Gender[recognition_performance$Subject == subj & recognition_performance$Timepoint == tp]
  memory_task_complete$Intervention[i] = recognition_performance$Intervention[recognition_performance$Subject == subj & recognition_performance$Timepoint == tp]
}

for(i in 1:length(memory_task_liking$Subject)){
  subj = memory_task_liking$Subject[i]
  tp = memory_task_liking$Timepoint[i]
  memory_task_liking$Age[i] = recognition_performance$Age[recognition_performance$Subject == subj & recognition_performance$Timepoint == tp]
  memory_task_liking$Gender[i] = recognition_performance$Gender[recognition_performance$Subject == subj & recognition_performance$Timepoint == tp]
  memory_task_liking$Intervention[i] = recognition_performance$Intervention[recognition_performance$Subject == subj & recognition_performance$Timepoint == tp]
}



### export data -------
saveRDS(memory_task_complete, paste0(working_dir, "memory_task_output_correctedSDT_with_wanting_and_image_characteristica_and_single_responses_after_exclusion.RDS"))
saveRDS(memory_task_liking, paste0(working_dir, "memory_task_output_correctedSDT_with_wanting_and_liking_and_image_characteristica_and_single_responses_after_exclusion.RDS"))



### laod data -----
memory_task_complete <- readRDS( paste0(working_dir, "memory_task_output_correctedSDT_with_wanting_and_image_characteristica_and_single_responses_after_exclusion.RDS"))

### factorize ----
memory_task_complete$Session <- factor(memory_task_complete$Session)
memory_task_complete$Gender <- factor(memory_task_complete$Gender)
memory_task_complete$Intervention <- factor(memory_task_complete$Intervention)
memory_task_complete$Category <- factor(memory_task_complete$Category, levels=c("NF", "F"))
memory_task_complete$Status <- factor(memory_task_complete$Status)
memory_task_complete$Type <- factor(memory_task_complete$Type)
memory_task_complete$Image_Number <- factor(memory_task_complete$Image_Number)
memory_task_complete$Style <- factor(memory_task_complete$Style)
memory_task_complete$Code <- factor(memory_task_complete$Code)
memory_task_complete$Wanting <- as.numeric(memory_task_complete$Wanting)
memory_task_complete$response_correct <- factor(memory_task_complete$response_correct)

### prepare for correlations -----
memory_task_complete_numeric <- memory_task_complete
for(i in 1:length(memory_task_complete)){
  memory_task_complete_numeric[,i] <- as.numeric(memory_task_complete[,i])
}
memory_task_complete_numeric <- memory_task_complete_numeric[,-c(1,3,4,8,12,14)]


### check correlations -----
memory_task_cor <- cor(memory_task_complete_numeric, use="pairwise.complete.obs", method = "spearman")

png("Figures/Correlations/single_item_correlation_matrix.png",         
    width = 1024, height = 1024, 
    unit="px",
    pointsize=20,
    bg = "white")          # Background color
corrplot::corrplot(memory_task_cor)
dev.off() 

### plots ------

ggplot(memory_task_complete, aes(x = z_Valence, y=response_correct)) +
  geom_boxplot(width=0.4, alpha=0.8, fill=met.brewer("Degas", n=7)[c(2,5)])+
  labs(title="Response accuracy depending on Valence")+
  ylab("Response accuracy") +
  xlab("Valence")

ggsave("Figures/Response_accuracy/Response_accuracy_valence.png",
       plot = last_plot(),
       device = "png",
       width = 15,
       height = 10,
       units = c("cm"),
       dpi = 300,
       bg = "white")   

ggplot(memory_task_complete, aes(x = z_Arousal, y=response_correct)) +
  geom_boxplot(width=0.4, alpha=0.8, fill=met.brewer("Degas", n=7)[c(2,5)])+
  labs(title="Response accuracy depending on Arousal")+
  ylab("Response accuracy") +
  xlab("Arousal")

ggsave("Figures/Response_accuracy/Response_accuracy_arousal.png",
       plot = last_plot(),
       device = "png",
       width = 15,
       height = 10,
       units = c("cm"),
       dpi = 300,
       bg = "white")   

ggplot(memory_task_complete, aes(x = z_Recognizability, y=response_correct)) +
  geom_boxplot(width=0.4, alpha=0.8, fill=met.brewer("Degas", n=7)[c(2,5)])+
  labs(title="Response accuracy depending on Recognizability")+
  ylab("Response accuracy") +
  xlab("Recognizability")

ggsave("Figures/Response_accuracy/Response_accuracy_recognizability.png",
       plot = last_plot(),
       device = "png",
       width = 15,
       height = 10,
       units = c("cm"),
       dpi = 300,
       bg = "white")   

ggplot(memory_task_complete, aes(x = z_Familiarity, y=response_correct)) +
  geom_boxplot(width=0.4, alpha=0.8, fill=met.brewer("Degas", n=7)[c(2,5)], outlier.shape = NA)+
  labs(title="Response accuracy depending on Familiarity")+
  ylab("Response accuracy") +
  xlab("Familiarity, z-scored") +
  xlim(c(-1.5,1))

ggsave("Figures/Response_accuracy/Response_accuracy_familiarity_food_only.png",
       plot = last_plot(),
       device = "png",
       width = 15,
       height = 10,
       units = c("cm"),
       dpi = 300,
       bg = "white")   

ggplot(memory_task_complete, aes(x = complexity_norm, y=response_correct)) +
  geom_boxplot(width=0.4, alpha=0.8, fill=met.brewer("Degas", n=7)[c(2,5)])+
  labs(title="Response accuracy depending on normed complexity")+
  ylab("Response accuracy") +
  xlab("Normed Complexity")

ggsave("Figures/Response_accuracy/Response_accuracy_complexity_norm.png",
       plot = last_plot(),
       device = "png",
       width = 15,
       height = 10,
       units = c("cm"),
       dpi = 300,
       bg = "white")   

ggplot(memory_task_complete, aes(x = complexity_norm, y=response_correct)) +
  geom_boxplot(width=0.4, alpha=0.8, fill=rep(met.brewer("Degas", n=7)[c(2,5)],2))+
  facet_grid(Category ~ .) +
  labs(title="Response accuracy depending on normed complexity per category")+
  ylab("Response accuracy") +
  xlab("Normed Complexity") 

ggsave("Figures/Response_accuracy/Response_accuracy_complexity_norm_category.png",
       plot = last_plot(),
       device = "png",
       width = 15,
       height = 15,
       units = c("cm"),
       dpi = 300,
       bg = "white")   

ggplot(memory_task_complete, aes(x = response_correct, fill = Category)) + 
  geom_histogram(stat="count", position = "dodge", binwidth = .4, color="black") +
  scale_fill_manual(values = rep(met.brewer("Lakota", n=2),2))

ggsave("Figures/Response_accuracy/Response_accuracy_category_histogram.png",
       plot = last_plot(),
       device = "png",
       width = 10,
       height = 10,
       units = c("cm"),
       dpi = 300,
       bg = "white") 

ggplot(memory_task_complete, aes(x = Wanting, fill=Category)) + 
  geom_histogram(stat="bin", position="dodge", binwidth = .5, color="black") +
  facet_grid(Category ~ response_correct) +
  scale_fill_manual(values = rep(met.brewer("Lakota", n=2),8))

ggsave("Figures/Response_accuracy/Response_accuracy_category_wanting_histogram.png",
       plot = last_plot(),
       device = "png",
       width = 10,
       height = 10,
       units = c("cm"),
       dpi = 300,
       bg = "white") 
