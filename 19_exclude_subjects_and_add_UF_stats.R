##### author: thieleking@cbs.mpg.de

### empty environment ------
rm(list=ls())



### set paths ------
working_dir = "/data/pt_02020/wd/Behav/memory/"
model_fits_dir = "/data/pt_02020/wd/Behav/memory/model_fits/"
model_comparisons_dir = "/data/pt_02020/Analysis/analysis_r_memory/Model_comparisons/"

### load dataframe ---------
recognition_performance_F_NF_wanting <- read.table(paste0(working_dir, "recognition_performance_F_NF_with_wanting_clean.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

discrimination_performance_F_NF_wanting <- read.table(paste0(working_dir, "discrimination_performance_F_NF_with_wanting_clean.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

recognition_performance_F_NF <- read.table(paste0(working_dir, "recognition_performance_F_NF_clean.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

discrimination_performance_F_NF <- read.table(paste0(working_dir, "discrimination_performance_F_NF_clean.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

recognition_performance <- read.table(paste0(working_dir, "recognition_performance_clean.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

discrimination_performance <- read.table(paste0(working_dir, "discrimination_performance_clean.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

UF_tract_stats = read.table("/data/pt_02020/MRI_data/DWI_results/tracking_UF/tract_statistics.csv", stringsAsFactors = F, sep = " ", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

###  remove subjects with low quality images -------

excluded_subjects_DWI <- data.frame(Subject=c("S09", "S29", "S30", "S45", "S59"),
                                    Session=c(2, 4, 1, 1, 4),
                                    Timepoint=NA, 
                                    Reason=c("motion artifacts", "motion artifacts", "motion artifacts", "motion artifacts", "no MRI due to COVID"))

excluded_subjects_DWI$Timepoint <- factor(excluded_subjects_DWI$Session, levels=1:4, labels=c("BL1", "FU1", "BL2", "FU2"))

for(sub in excluded_subjects_DWI$Subject){
  ses <- excluded_subjects_DWI$Session[excluded_subjects_DWI$Subject == sub]
  recognition_performance_F_NF_wanting <- recognition_performance_F_NF_wanting[!(recognition_performance_F_NF_wanting$Subject == sub & recognition_performance_F_NF_wanting$Session == ses),]
  recognition_performance_F_NF <- recognition_performance_F_NF[!(recognition_performance_F_NF$Subject == sub & recognition_performance_F_NF$Session == ses),]
  recognition_performance <- recognition_performance[!(recognition_performance$Subject == sub & recognition_performance$Session == ses),]
  discrimination_performance_F_NF_wanting <- discrimination_performance_F_NF_wanting[!(discrimination_performance_F_NF_wanting$Subject == sub & discrimination_performance_F_NF_wanting$Session == ses),]
  discrimination_performance_F_NF <- discrimination_performance_F_NF[!(discrimination_performance_F_NF$Subject == sub & discrimination_performance_F_NF$Session == ses),]
  discrimination_performance <- discrimination_performance[!(discrimination_performance$Subject == sub & discrimination_performance$Session == ses),]
}

### export dataframes -----
write.table(excluded_subjects_DWI, paste0(working_dir, "excluded_subjects_from_DWI.csv"), quote = FALSE, sep = "\t", row.names = F)



### add nqa values to memory dataframes -----
for(sub in unique(recognition_performance_F_NF_wanting$Subject)){
  for(ses in unique(recognition_performance_F_NF_wanting$Session[recognition_performance_F_NF_wanting$Subject== sub])){
    subject = paste0("sub-", sub(".*S", "", sub))
    session = paste0("ses-0", ses)
    recognition_performance_F_NF_wanting$nQA_UF_mean[recognition_performance_F_NF_wanting$Subject == sub & recognition_performance_F_NF_wanting$Session == ses] <- mean(c(UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "UF_L"], UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "UF_R"]))
    recognition_performance_F_NF_wanting$nQA_UF_L[recognition_performance_F_NF_wanting$Subject == sub & recognition_performance_F_NF_wanting$Session == ses] <- UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "UF_L"]
    recognition_performance_F_NF_wanting$nQA_UF_R[recognition_performance_F_NF_wanting$Subject == sub & recognition_performance_F_NF_wanting$Session == ses] <- UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "UF_R"]
    recognition_performance_F_NF_wanting$nQA_sub_UF_mean[recognition_performance_F_NF_wanting$Subject == sub & recognition_performance_F_NF_wanting$Session == ses] <- mean(c(UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "sub_UF_L"], UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "sub_UF_R"]))
    recognition_performance_F_NF_wanting$nQA_sub_UF_L[recognition_performance_F_NF_wanting$Subject == sub & recognition_performance_F_NF_wanting$Session == ses] <- UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "sub_UF_L"]
    recognition_performance_F_NF_wanting$nQA_sub_UF_R[recognition_performance_F_NF_wanting$Subject == sub & recognition_performance_F_NF_wanting$Session == ses] <- UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "sub_UF_R"]
  }
}

for(sub in unique(discrimination_performance_F_NF_wanting$Subject)){
  for(ses in unique(discrimination_performance_F_NF_wanting$Session[discrimination_performance_F_NF_wanting$Subject== sub])){
    subject = paste0("sub-", sub(".*S", "", sub))
    session = paste0("ses-0", ses)
    discrimination_performance_F_NF_wanting$nQA_UF_mean[discrimination_performance_F_NF_wanting$Subject == sub & discrimination_performance_F_NF_wanting$Session == ses] <- mean(c(UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "UF_L"], UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "UF_R"]))
    discrimination_performance_F_NF_wanting$nQA_UF_L[discrimination_performance_F_NF_wanting$Subject == sub & discrimination_performance_F_NF_wanting$Session == ses] <- UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "UF_L"]
    discrimination_performance_F_NF_wanting$nQA_UF_R[discrimination_performance_F_NF_wanting$Subject == sub & discrimination_performance_F_NF_wanting$Session == ses] <- UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "UF_R"]
    discrimination_performance_F_NF_wanting$nQA_sub_UF_mean[discrimination_performance_F_NF_wanting$Subject == sub & discrimination_performance_F_NF_wanting$Session == ses] <- mean(c(UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "sub_UF_L"], UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "sub_UF_R"]))
    discrimination_performance_F_NF_wanting$nQA_sub_UF_L[discrimination_performance_F_NF_wanting$Subject == sub & discrimination_performance_F_NF_wanting$Session == ses] <- UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "sub_UF_L"]
    discrimination_performance_F_NF_wanting$nQA_sub_UF_R[discrimination_performance_F_NF_wanting$Subject == sub & discrimination_performance_F_NF_wanting$Session == ses] <- UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "sub_UF_R"]
  }
}

for(sub in unique(recognition_performance_F_NF$Subject)){
  for(ses in unique(recognition_performance_F_NF$Session[recognition_performance_F_NF$Subject== sub])){
    subject = paste0("sub-", sub(".*S", "", sub))
    session = paste0("ses-0", ses)
    recognition_performance_F_NF$nQA_UF_mean[recognition_performance_F_NF$Subject == sub & recognition_performance_F_NF$Session == ses] <- mean(c(UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "UF_L"], UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "UF_R"]))
    recognition_performance_F_NF$nQA_UF_L[recognition_performance_F_NF$Subject == sub & recognition_performance_F_NF$Session == ses] <- UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "UF_L"]
    recognition_performance_F_NF$nQA_UF_R[recognition_performance_F_NF$Subject == sub & recognition_performance_F_NF$Session == ses] <- UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "UF_R"]
    recognition_performance_F_NF$nQA_sub_UF_mean[recognition_performance_F_NF$Subject == sub & recognition_performance_F_NF$Session == ses] <- mean(c(UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "sub_UF_L"], UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "sub_UF_R"]))
    recognition_performance_F_NF$nQA_sub_UF_L[recognition_performance_F_NF$Subject == sub & recognition_performance_F_NF$Session == ses] <- UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "sub_UF_L"]
    recognition_performance_F_NF$nQA_sub_UF_R[recognition_performance_F_NF$Subject == sub & recognition_performance_F_NF$Session == ses] <- UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "sub_UF_R"]
  }
}

for(sub in unique(discrimination_performance_F_NF$Subject)){
  for(ses in unique(discrimination_performance_F_NF$Session[discrimination_performance_F_NF$Subject== sub])){
    subject = paste0("sub-", sub(".*S", "", sub))
    session = paste0("ses-0", ses)
    discrimination_performance_F_NF$nQA_UF_mean[discrimination_performance_F_NF$Subject == sub & discrimination_performance_F_NF$Session == ses] <- mean(c(UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "UF_L"], UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "UF_R"]))
    discrimination_performance_F_NF$nQA_UF_L[discrimination_performance_F_NF$Subject == sub & discrimination_performance_F_NF$Session == ses] <- UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "UF_L"]
    discrimination_performance_F_NF$nQA_UF_R[discrimination_performance_F_NF$Subject == sub & discrimination_performance_F_NF$Session == ses] <- UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "UF_R"]
    discrimination_performance_F_NF$nQA_sub_UF_mean[discrimination_performance_F_NF$Subject == sub & discrimination_performance_F_NF$Session == ses] <- mean(c(UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "sub_UF_L"], UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "sub_UF_R"]))
    discrimination_performance_F_NF$nQA_sub_UF_L[discrimination_performance_F_NF$Subject == sub & discrimination_performance_F_NF$Session == ses] <- UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "sub_UF_L"]
    discrimination_performance_F_NF$nQA_sub_UF_R[discrimination_performance_F_NF$Subject == sub & discrimination_performance_F_NF$Session == ses] <- UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "sub_UF_R"]
  }
}

for(sub in unique(recognition_performance$Subject)){
  for(ses in unique(recognition_performance$Session[recognition_performance$Subject== sub])){
    subject = paste0("sub-", sub(".*S", "", sub))
    session = paste0("ses-0", ses)
    recognition_performance$nQA_UF_mean[recognition_performance$Subject == sub & recognition_performance$Session == ses] <- mean(c(UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "UF_L"], UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "UF_R"]))
    recognition_performance$nQA_UF_L[recognition_performance$Subject == sub & recognition_performance$Session == ses] <- UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "UF_L"]
    recognition_performance$nQA_UF_R[recognition_performance$Subject == sub & recognition_performance$Session == ses] <- UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "UF_R"]
    recognition_performance$nQA_sub_UF_mean[recognition_performance$Subject == sub & recognition_performance$Session == ses] <- mean(c(UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "sub_UF_L"], UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "sub_UF_R"]))
    recognition_performance$nQA_sub_UF_L[recognition_performance$Subject == sub & recognition_performance$Session == ses] <- UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "sub_UF_L"]
    recognition_performance$nQA_sub_UF_R[recognition_performance$Subject == sub & recognition_performance$Session == ses] <- UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "sub_UF_R"]
  }
}

for(sub in unique(discrimination_performance$Subject)){
  for(ses in unique(discrimination_performance$Session[discrimination_performance$Subject== sub])){
    subject = paste0("sub-", sub(".*S", "", sub))
    session = paste0("ses-0", ses)
    discrimination_performance$nQA_UF_mean[discrimination_performance$Subject == sub & discrimination_performance$Session == ses] <- mean(c(UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "UF_L"], UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "UF_R"]))
    discrimination_performance$nQA_UF_L[discrimination_performance$Subject == sub & discrimination_performance$Session == ses] <- UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "UF_L"]
    discrimination_performance$nQA_UF_R[discrimination_performance$Subject == sub & discrimination_performance$Session == ses] <- UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "UF_R"]
    discrimination_performance$nQA_sub_UF_mean[discrimination_performance$Subject == sub & discrimination_performance$Session == ses] <- mean(c(UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "sub_UF_L"], UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "sub_UF_R"]))
    discrimination_performance$nQA_sub_UF_L[discrimination_performance$Subject == sub & discrimination_performance$Session == ses] <- UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "sub_UF_L"]
    discrimination_performance$nQA_sub_UF_R[discrimination_performance$Subject == sub & discrimination_performance$Session == ses] <- UF_tract_stats$nqa[UF_tract_stats$subj == subject & UF_tract_stats$ses == session & UF_tract_stats$tract == "sub_UF_R"]
  }
}

### export dataframes -------
write.table(recognition_performance_F_NF_wanting, paste0(working_dir, "recognition_performance_F_NF_with_wanting_clean_DWI.csv"), quote = FALSE, sep = "\t", row.names = F)
write.table(discrimination_performance_F_NF_wanting, paste0(working_dir, "discrimination_performance_F_NF_with_wanting_clean_DWI.csv"), quote = FALSE, sep = "\t", row.names = F)

write.table(recognition_performance_F_NF, paste0(working_dir, "recognition_performance_F_NF_clean_DWI.csv"), quote = FALSE, sep = "\t", row.names = F)
write.table(discrimination_performance_F_NF, paste0(working_dir, "discrimination_performance_F_NF_clean_DWI.csv"), quote = FALSE, sep = "\t", row.names = F)

write.table(recognition_performance, paste0(working_dir, "recognition_performance_clean_DWI.csv"), quote = FALSE, sep = "\t", row.names = F)
write.table(discrimination_performance, paste0(working_dir, "discrimination_performance_clean_DWI.csv"), quote = FALSE, sep = "\t", row.names = F)

