##### author: thieleking@cbs.mpg.de

### empty environment ------
rm(list=ls())

### load packages -------
library(rstatix)
library(ggpubr)

### set paths ------
working_dir = "/data/pt_02020/wd/Behav/memory/"

### load dataframe ---------
recognition_performance_F_NF <- read.table(paste0(working_dir, "recognition_performance_F_NF_clean.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

discrimination_performance_F_NF <- read.table(paste0(working_dir, "discrimination_performance_F_NF_clean.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

recognition_performance_F_NF_wanting <- read.table(paste0(working_dir, "recognition_performance_F_NF_with_wanting_clean.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

discrimination_performance_F_NF_wanting <- read.table(paste0(working_dir, "discrimination_performance_F_NF_with_wanting_clean.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

### check empty wanting cats -------
no_tp=0
for(subj in unique(recognition_performance_F_NF_wanting$Subject)){
  for(ses in unique(recognition_performance_F_NF_wanting$Session[recognition_performance_F_NF_wanting$Subject == subj])){
    if(any(is.na(recognition_performance_F_NF_wanting$dprime[recognition_performance_F_NF_wanting$Subject == subj & recognition_performance_F_NF_wanting$Session == ses]))){
      print(recognition_performance_F_NF_wanting[recognition_performance_F_NF_wanting$Subject == subj & recognition_performance_F_NF_wanting$Session == ses,c(1:2,13:14,20:24)])
      no_tp=no_tp+1
    }
    
  }
}
print(paste0("amount of timepoints with at least one empty wanting category: ", no_tp))

### test for normality ----
recognition_performance_F_NF_wanting %>%
  group_by(Category, Wanting_cat) %>%
  shapiro_test(dprime)
## --> only "F, neutral" and "NF, unwanted" not normally distributed

discrimination_performance_F_NF_wanting %>%
  group_by(Category, Wanting_cat) %>%
  shapiro_test(LDI)
## all normally distributed

### visual check for normality -----
for(cat in c("F", "NF")){
  for(wanting in c("unwanted", "neutral", "wanted")){
    print(ggqqplot(recognition_performance_F_NF_wanting$dprime[recognition_performance_F_NF_wanting$Category == cat & recognition_performance_F_NF_wanting$Wanting_cat == wanting]) +
            ggtitle(paste0("d', ", cat, " + ", wanting)))
  }
}

for(cat in c("F", "NF")){
  for(wanting in c("unwanted", "neutral", "wanted")){
    print(ggqqplot(discrimination_performance_F_NF_wanting$LDI[discrimination_performance_F_NF_wanting$Category == cat & discrimination_performance_F_NF_wanting$Wanting_cat == wanting]) +
            ggtitle(paste0("LDI, ", cat, " + ", wanting)))
  }
}

### check completeness -----
all_subs=c(paste0("S", sprintf("%02d",1:61)))
all_sessions=c(1:4)

`%!in%` <- Negate(`%in%`)

for(subj in all_subs){
  if(subj %!in% unique(recognition_performance_F_NF$Subject)){
    print(paste0(subj," is completely excluded."))
  }
  else{
    for(ses in all_sessions){
      if(ses %!in% unique(recognition_performance_F_NF$Session[recognition_performance_F_NF$Subject == subj])){
        print(paste0(subj, " Session ", ses, " does not enter memory analyses."))
      }
    }
  }
}
