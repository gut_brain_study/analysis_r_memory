##### author: thieleking@cbs.mpg.de

### load packages -------
library(magrittr)
library(ggplot2)
library(tibble)
library(visibly)
library(tidybayes)
library(ggpubr)
library(MetBrewer)
library(sjPlot)

### set paths ------
working_dir = "/data/pt_02020/wd/Behav/memory/"
model_fits_dir = "/data/pt_02020/wd/Behav/memory/model_fits/"
model_comparisons_dir = "/data/pt_02020/Analysis/analysis_r_memory/Model_comparisons/"

### load dataframe ---------
recognition_performance_F_NF_wanting <- read.table(paste0(working_dir, "recognition_performance_F_NF_with_wanting_clean.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

discrimination_performance_F_NF_wanting <- read.table(paste0(working_dir, "discrimination_performance_F_NF_with_wanting_clean.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

### factorize ------
recognition_performance_F_NF_wanting$Wanting_cat <- factor(recognition_performance_F_NF_wanting$Wanting_cat, levels=c("unwanted", "neutral", "wanted"))
discrimination_performance_F_NF_wanting$Wanting_cat <- factor(discrimination_performance_F_NF_wanting$Wanting_cat, levels=c("unwanted", "neutral", "wanted"))

### load model fits ----------
full.dprime.category.wanting.allrandomeffects <- readRDS(paste0(model_fits_dir, "Wanting_TPxIntervention/full.dprime.category.wanting.allrandomeffects.RDS"))
full.ldi.category.wanting.allrandomeffects <- readRDS(paste0(model_fits_dir, "Wanting_TPxIntervention/full.ldi.category.wanting.allrandomeffects.RDS"))
null.dprime.category.wanting.allrandomeffects <- readRDS(paste0(model_fits_dir, "Wanting_TPxIntervention/null.dprime.category.wanting.allrandomeffects.RDS"))
null.ldi.category.wanting.allrandomeffects <- readRDS(paste0(model_fits_dir, "Wanting_TPxIntervention/null.ldi.category.wanting.allrandomeffects.RDS"))
null.dprime.category.allrandomeffects <- readRDS(paste0(model_fits_dir, "Wanting_TPxIntervention/null.dprime.category.allrandomeffects.RDS"))
null.ldi.category.allrandomeffects <- readRDS(paste0(model_fits_dir, "Wanting_TPxIntervention/null.ldi.category.allrandomeffects.RDS"))
null.dprime.allrandomeffects <- readRDS(paste0(model_fits_dir, "Wanting_TPxIntervention/null.dprime.allrandomeffects.RDS"))
null.ldi.allrandomeffects <- readRDS(paste0(model_fits_dir, "Wanting_TPxIntervention/null.ldi.allrandomeffects.RDS"))

### plots --------
##### set theme for sjPlot --------
set_theme(
  title.size = 2, 
  axis.title.size = 1.7,
  axis.title.color = "black",
  axis.textsize = 1.3,
  axis.textcolor = "grey30", 
  legend.size = 1.3,
  legend.title.size = 1.7,
  geom.label.size =5,
  base = theme_bw()
)

##### dprime wanting -------
plot_model(null.dprime.category.wanting.allrandomeffects, type = "pred", terms = c("Wanting_cat"), colors = met.brewer("Austria", n=6)[1], axis.lim=c(0,3.0), title="Target recognition predicted by Wanting", axis.title=c("Wanting Category", "d'"))  +
  aes(color="Wanting_cat") +
  theme(legend.position = "none") + 
  annotate("text", x=3, y = 2.9,
           label = expression(beta ~ ("Wanting Category (neutral > unwanted)") ~ " =  0.04 [-0.07, 0.14]"), hjust =1) +
  annotate("text", x=3, y = 2.7,
           label = expression(beta ~ ("Wanting Category (wanted > unwanted)") ~ " =  -0.10 [-0.22, 0.02]"), hjust =1) +
  annotate("text", x=3, y = 2.5,
           label = expression(beta ~ ("Image Category (F > NF)") ~ " =  0.67 [0.09, 1.18] *"), hjust =1)

ggsave("Figures/Wanting/dprime_prediction_by_wanting.png",
       plot = last_plot(),
       device = "png",
       width = 20,
       height = 15,
       units = c("cm"),
       dpi = 300,
       bg = "white") 

##### dprime wanting x category -------
plot_model(full.dprime.category.wanting.allrandomeffects, type = "pred", terms = c("Wanting_cat", "Category"), colors = met.brewer("Lakota", n=2)[1:2], axis.lim=c(0,3.3), title="Target recognition predicted by Wanting Category", axis.title=c("Wanting Category", "d'"), dot.size = 3, line.size = 2)  +
  #geom_violin(aes(x=Wanting_cat, y=dprime, group=Category, col=Category, alpha=0.5), recognition_performance_F_NF_wanting, position = position_dodge(0.4), inherit.aes = F, fill="darkgrey") +
  #geom_point(aes(x=Wanting_cat, y=dprime, group=Category, col=Category, alpha=0.5), recognition_performance_F_NF_wanting, position = position_dodge(0.4), inherit.aes = F) +
  annotate("text", x=3, y = 3.3,
           label = expression(beta ~ ("Wanting Category (neutral > unwanted) * Image Category (F > NF)") ~ " =  -0.11 [-0.30, 0.07]"), hjust =1) +
  annotate("text", x=3, y = 3.1,
           label = expression(beta ~ ("Wanting Category (wanted > unwanted) * Image Category (F > NF)") ~ " =  -0.04 [-0.25, 0.17]"), hjust =1) +
  annotate("text", x=3, y = 2.9,
           label = expression(beta ~ ("Wanting Category (neutral > unwanted)") ~ " =  0.09 [-0.05, 0.23]"), hjust =1) +
  annotate("text", x=3, y = 2.7,
           label = expression(beta ~ ("Wanting Category (wanted > unwanted)") ~ " =  -0.08 [-0.23, 0.07]"), hjust =1)

ggsave("Figures/Wanting/dprime_prediction_by_wanting_per_category.png",
       plot = last_plot(),
       device = "png",
       width = 28,
       height = 15,
       units = c("cm"),
       dpi = 300,
       bg = "white") 

##### ldi wanting -------
plot_model(null.ldi.category.wanting.allrandomeffects, type = "pred", terms = c("Wanting_cat"), colors = met.brewer("Austria", n=6)[1], axis.lim=c(0,3.0), title="Lure discrimination predicted by Wanting", axis.title=c("Wanting Category", "LDI"))  +
  aes(color="Wanting_cat") +
  theme(legend.position = "none") + 
  annotate("text", x=3, y = 2.9,
           label = expression(beta ~ ("Wanting Category (neutral > unwanted)") ~ " =  0.04 [-0.07, 0.15]"), hjust =1) +
  annotate("text", x=3, y = 2.7,
           label = expression(beta ~ ("Wanting Category (wanted > unwanted)") ~ " =  -0.01 [-0.14, 0.12]"), hjust =1) +
  annotate("text", x=3, y = 2.5,
           label = expression(beta ~ ("Image Category (F > NF)") ~ " =  0.74 [0.20, 1.29] *"), hjust =1)

ggsave("Figures/Wanting/ldi_prediction_by_wanting.png",
       plot = last_plot(),
       device = "png",
       width = 20,
       height = 15,
       units = c("cm"),
       dpi = 300,
       bg = "white") 

##### ldi wanting x category -------
plot_model(full.ldi.category.wanting.allrandomeffects, type = "pred", terms = c("Wanting_cat", "Category"), colors = met.brewer("Lakota", n=2)[1:2], axis.lim=c(0,3.3), title="Lure discrimination predicted by Wanting Category", axis.title=c("Wanting Category", "LDI"), dot.size = 3, line.size = 2) +
  annotate("text", x=3, y = 3.3,
           label = expression(beta ~ ("Wanting Category (neutral > unwanted) * Image Category (F > NF)") ~ " =  -0.12 [-0.33, 0.08]"), hjust =1) +
  annotate("text", x=3, y = 3.1,
           label = expression(beta ~ ("Wanting Category (wanted > unwanted) * Image Category (F > NF)") ~ " =  -0.09 [-0.32, 0.14]"), hjust =1) +
  annotate("text", x=3, y = 2.9,
           label = expression(beta ~ ("Wanting Category (neutral > unwanted)") ~ " =  -0.13 [-0.33, 0.07]"), hjust =1) +
  annotate("text", x=3, y = 2.7,
           label = expression(beta ~ ("Wanting Category (wanted > unwanted)") ~ " =  -0.08 [-0.31, 0.14]"), hjust =1)

ggsave("Figures/Wanting/ldi_prediction_by_wanting_per_category.png",
       plot = last_plot(),
       device = "png",
       width = 28,
       height = 15,
       units = c("cm"),
       dpi = 300,
       bg = "white") 

### plot model results -----

plot_model(full.dprime.category.wanting.allrandomeffects, 
           vline.color = "black", 
           show.values = TRUE, 
           value.offset = .3,
           value.size = 5,
           bpe = "mean",
           bpe.style = "dot",
           bpe.color = "white",
           dot.size = 2,
           line.size = 1,
           ci.style = "whisker",
           prob.inner = .5,
           prob.outer = .95,
           auto.label=F,
           title = "d'",
           axis.title = "Estimates",
           axis.labels=rev(c("Category (F > NF)", "Wanting Category (neutral > unwanted)","Wanting Category (wanted > unwanted)",  "Gender (M > F)", "Age",
                             "Timepoint (FU > BL)", "Intervention (Fiber > Placebo)", "Category (F > NF) * Wanting Category (neutral > unwanted)", "Category (F > NF) * Wanting Category (wanted > unwanted)", "Timepoint * Intervention")))

ggsave("Figures/Wanting/dprime_wanting_full_model_results.png",
       plot = last_plot(),
       device = "png",
       width = 30,
       height = 25,
       units = c("cm"),
       dpi = 300,
       bg = "white") 

plot_model(full.ldi.category.wanting.allrandomeffects, 
           vline.color = "black", 
           show.values = TRUE, 
           value.offset = .3,
           value.size = 5,
           bpe = "mean",
           bpe.style = "dot",
           bpe.color = "white",
           dot.size = 2,
           line.size = 1,
           ci.style = "whisker",
           prob.inner = .5,
           prob.outer = .95,
           auto.label=F,
           title = "LDI",
           axis.title = "Estimates",
           axis.labels=rev(c("Category (F > NF)", "Wanting Category (neutral > unwanted)","Wanting Category (wanted > unwanted)",  "Gender (M > F)", "Age",
                             "Timepoint (FU > BL)", "Intervention (Fiber > Placebo)", "Category (F > NF) * Wanting Category (neutral > unwanted)", "Category (F > NF) * Wanting Category (wanted > unwanted)", "Timepoint * Intervention")))


ggsave("Figures/Wanting/ldi_wanting_full_model_results.png",
       plot = last_plot(),
       device = "png",
       width = 30,
       height = 25,
       units = c("cm"),
       dpi = 300,
       bg = "white") 


plot_model(null.dprime.category.wanting.allrandomeffects, 
           vline.color = "black", 
           show.values = TRUE, 
           value.offset = .3,
           value.size = 5,
           bpe = "mean",
           bpe.style = "dot",
           bpe.color = "white",
           dot.size = 2,
           line.size = 1,
           ci.style = "whisker",
           prob.inner = .5,
           prob.outer = .95,
           auto.label=F,
           title = "d'",
           axis.labels=rev(c("Category (F > NF)", "Wanting Category (neutral > unwanted)","Wanting Category (wanted > unwanted)",  "Gender (M > F)", "Age",
                             "Timepoint (FU > BL)", "Intervention (Fiber > Placebo)", "Timepoint * Intervention")))


ggsave("Figures/Wanting/dprime_wanting_null_model_no_interaction_results.png",
       plot = last_plot(),
       device = "png",
       width = 28,
       height = 20,
       units = c("cm"),
       dpi = 300,
       bg = "white") 

plot_model(null.ldi.category.wanting.allrandomeffects, 
           vline.color = "black", 
           show.values = TRUE, 
           value.offset = .3,
           value.size = 5,
           bpe = "mean",
           bpe.style = "dot",
           bpe.color = "white",
           dot.size = 2,
           line.size = 1,
           ci.style = "whisker",
           prob.inner = .5,
           prob.outer = .95,
           auto.label=F,
           title = "LDI",
           axis.labels=rev(c("Category (F > NF)", "Wanting Category (neutral > unwanted)","Wanting Category (wanted > unwanted)",  "Gender (M > F)", "Age",
                             "Timepoint (FU > BL)", "Intervention (Fiber > Placebo)", "Timepoint * Intervention")))


ggsave("Figures/Wanting/ldi_wanting_null_model_no_interaction_results.png",
       plot = last_plot(),
       device = "png",
       width = 28,
       height = 20,
       units = c("cm"),
       dpi = 300,
       bg = "white") 

plot_model(null.dprime.category.allrandomeffects, 
           vline.color = "black", 
           show.values = TRUE, 
           value.offset = .3,
           value.size = 5,
           bpe = "mean",
           bpe.style = "dot",
           bpe.color = "white",
           dot.size = 2,
           line.size = 1,
           ci.style = "whisker",
           prob.inner = .5,
           prob.outer = .95,
           auto.label=F,
           title = "d'",
           axis.labels=rev(c("Category (F > NF)", "Gender (M > F)", "Age",
                             "Timepoint (FU > BL)", "Intervention (Fiber > Placebo)", "Timepoint * Intervention")))

ggsave("Figures/Wanting/dprime_wanting_null_model_only_category_results.png",
       plot = last_plot(),
       device = "png",
       width = 28,
       height = 18,
       units = c("cm"),
       dpi = 300,
       bg = "white") 

plot_model(null.ldi.category.allrandomeffects, 
           vline.color = "black", 
           show.values = TRUE, 
           value.offset = .3,
           value.size = 5,
           bpe = "mean",
           bpe.style = "dot",
           bpe.color = "white",
           dot.size = 2,
           line.size = 1,
           ci.style = "whisker",
           prob.inner = .5,
           prob.outer = .95,
           auto.label=F,
           title = "LDI",
           axis.labels=rev(c("Category (F > NF)", "Gender (M > F)", "Age",
                             "Timepoint (FU > BL)", "Intervention (Fiber > Placebo)", "Timepoint * Intervention")))

ggsave("Figures/Wanting/ldi_wanting_null_model_only_category_results.png",
       plot = last_plot(),
       device = "png",
       width = 28,
       height = 18,
       units = c("cm"),
       dpi = 300,
       bg = "white") 

### residuals plots --------
##### dprime ------
res_full_dprime <- recognition_performance_F_NF_wanting %>%
  add_residual_draws(full.dprime.category.wanting.allrandomeffects) %>%
  ggplot(aes(x = .row, y = .residual)) +
  stat_pointinterval() +
  geom_hline(yintercept = 0, colour="red") +
  xlab("Data point") + ylab("Residuals") +
  labs(title="Residuals of model fits", subtitle="d' full model with category * wanting")

res_null_cat_want_dprime <- recognition_performance_F_NF_wanting %>%
  add_residual_draws(null.dprime.category.wanting.allrandomeffects) %>%
  ggplot(aes(x = .row, y = .residual)) +
  stat_pointinterval() +
  geom_hline(yintercept = 0, colour="red") +
  xlab("Data point")  + ylab("Residuals") +
  labs(subtitle="d' null model with category + wanting")

res_null_cat_dprime <- recognition_performance_F_NF_wanting %>%
  add_residual_draws(null.dprime.category.wanting.allrandomeffects) %>%
  ggplot(aes(x = .row, y = .residual)) +
  stat_pointinterval()+
  geom_hline(yintercept = 0, colour="red") +
  xlab("Data point") + ylab("Residuals") +
  labs(subtitle="d' null model with category")

ggarrange(res_full_dprime, res_null_cat_want_dprime, res_null_cat_dprime, nrow=3)

ggsave("Figures/Wanting/dprime_residuals.png",
       plot = last_plot(),
       device = "png",
       width = 20,
       height = 30,
       units = c("cm"),
       dpi = 300,
       bg = "white") 

##### ldi ------
res_full_ldi <- discrimination_performance_F_NF_wanting %>%
  add_residual_draws(full.ldi.category.wanting.allrandomeffects) %>%
  ggplot(aes(x = .row, y = .residual)) +
  stat_pointinterval() +
  geom_hline(yintercept = 0, colour="red") +
  xlab("Data point")  + ylab("Residuals") +
  labs(title="Residuals of model fits", subtitle="LDI full model with category * wanting")

res_null_cat_want_ldi <- discrimination_performance_F_NF_wanting %>%
  add_residual_draws(null.ldi.category.wanting.allrandomeffects) %>%
  ggplot(aes(x = .row, y = .residual)) +
  stat_pointinterval() +
  geom_hline(yintercept = 0, colour="red") +
  xlab("Data point") + ylab("Residuals") +
  labs(subtitle="LDI null model with category + wanting")

res_null_cat_ldi <- discrimination_performance_F_NF_wanting %>%
  add_residual_draws(null.ldi.category.wanting.allrandomeffects) %>%
  ggplot(aes(x = .row, y = .residual)) +
  stat_pointinterval()+
  geom_hline(yintercept = 0, colour="red") +
  xlab("Data point") + ylab("Residuals") +
  labs(subtitle="LDI null model with category")

ggarrange(res_full_ldi, res_null_cat_want_ldi, res_null_cat_ldi, nrow=3)

ggsave("Figures/Wanting/ldi_residuals.png",
       plot = last_plot(),
       device = "png",
       width = 20,
       height = 30,
       units = c("cm"),
       dpi = 300,
       bg = "white") 

### actual vs fitted value ------------
##### dprime ------
fitted_full_dprime <- fitted(full.dprime.category.wanting.allrandomeffects)

data_full_prime <- as.data.frame(cbind(dprime = recognition_performance_F_NF_wanting$dprime[!is.na(recognition_performance_F_NF_wanting$dprime)], fitted_full_dprime))

fit_full_dprime <- ggplot(data_full_prime) + 
  geom_point(aes(x = Estimate, y = dprime)) +
  geom_abline(intercept=0, slope=1, colour="red") +
  xlab("Actual d' value") + 
  ylab("Fitted d' value") +
  scale_x_continuous(limits = c( 0,3 )) +
  scale_y_continuous(limits = c(-1, 4)) +
  labs(title="Fitted vs actual d' value", subtitle="d' full model with category * wanting")

fitted_null_cat_want_dprime <- fitted(null.dprime.category.wanting.allrandomeffects)

data_null_cat_want_prime <- as.data.frame(cbind(dprime = recognition_performance_F_NF_wanting$dprime[!is.na(recognition_performance_F_NF_wanting$dprime)], fitted_null_cat_want_dprime))

fit_null_cat_want_dprime <- ggplot(data_null_cat_want_prime) + 
  geom_point(aes(x = Estimate, y = dprime)) +
  geom_abline(intercept=0, slope=1, colour="red") +
  xlab("Actual d' value") + 
  ylab("Fitted d' value") +
  scale_x_continuous(limits = c( 0,3 )) +
  scale_y_continuous(limits = c(-1, 4)) +
  labs(subtitle="d' null model with category + wanting")

fitted_null_cat_dprime <- fitted(null.dprime.category.allrandomeffects)

data_null_cat_dprime <- as.data.frame(cbind(dprime = recognition_performance_F_NF_wanting$dprime[!is.na(recognition_performance_F_NF_wanting$dprime)], fitted_null_cat_dprime))

fit_null_cat_dprime <- ggplot(data_null_cat_dprime) + 
  geom_point(aes(x = Estimate, y = dprime)) +
  geom_abline(intercept=0, slope=1, colour="red") +
  xlab("Actual d' value") + 
  ylab("Fitted d' value") +
  scale_x_continuous(limits = c( 0,3 )) +
  scale_y_continuous(limits = c(-1, 4)) +
  labs(subtitle="d' null model with category")

ggarrange(fit_full_dprime, fit_null_cat_want_dprime, fit_null_cat_dprime, nrow=3)

ggsave("Figures/Wanting/dprime_fitted_actual.png",
       plot = last_plot(),
       device = "png",
       width = 20,
       height = 30,
       units = c("cm"),
       dpi = 300,
       bg = "white") 

##### ldi ------
fitted_full_ldi <- fitted(full.ldi.category.wanting.allrandomeffects)

data_full_ldi <- as.data.frame(cbind(ldi = discrimination_performance_F_NF_wanting$LDI[!is.na(discrimination_performance_F_NF_wanting$LDI)], fitted_full_ldi))

fit_full_ldi <- ggplot(data_full_ldi) + 
  geom_point(aes(x = Estimate, y = ldi)) +
  geom_abline(intercept=0, slope=1, colour="red") +
  xlab("Actual LDI value") + 
  ylab("Fitted LDI value") +
  scale_x_continuous(limits = c( -.2,3 )) +
  scale_y_continuous(limits = c(-2, 4)) +
  labs(title="Fitted vs actual LDI value", subtitle="LDI full model with category * wanting")

fitted_null_cat_want_ldi <- fitted(null.ldi.category.wanting.allrandomeffects)

data_null_cat_want_prime <- as.data.frame(cbind(ldi = discrimination_performance_F_NF_wanting$LDI[!is.na(discrimination_performance_F_NF_wanting$LDI)], fitted_null_cat_want_ldi))

fit_null_cat_want_ldi <- ggplot(data_null_cat_want_prime) + 
  geom_point(aes(x = Estimate, y = ldi)) +
  geom_abline(intercept=0, slope=1, colour="red") +
  xlab("Actual LDI value") + 
  ylab("Fitted LDI value") +
  scale_x_continuous(limits = c( -.2,3 )) +
  scale_y_continuous(limits = c(-2, 4)) +
  labs(subtitle="LDI null model with category + wanting")

fitted_null_cat_ldi <- fitted(null.ldi.category.allrandomeffects)

data_null_cat_prime <- as.data.frame(cbind(ldi = discrimination_performance_F_NF_wanting$LDI[!is.na(discrimination_performance_F_NF_wanting$LDI)], fitted_null_cat_ldi))

fit_null_cat_ldi <- ggplot(data_null_cat_prime) + 
  geom_point(aes(x = Estimate, y = ldi)) +
  geom_abline(intercept=0, slope=1, colour="red") +
  xlab("Actual LDI value") + 
  ylab("Fitted LDI value") +
  scale_x_continuous(limits = c( -.2,3 )) +
  scale_y_continuous(limits = c(-2, 4)) +
  labs(subtitle="LDI null model with category")

ggarrange(fit_full_ldi, fit_null_cat_want_ldi, fit_null_cat_ldi, nrow=3)

ggsave("Figures/Wanting/ldi_fitted_actual.png",
       plot = last_plot(),
       device = "png",
       width = 20,
       height = 30,
       units = c("cm"),
       dpi = 300,
       bg = "white") 
