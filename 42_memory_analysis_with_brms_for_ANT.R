##### author: thieleking@cbs.mpg.de

### empty environment ------
rm(list=ls())

### load packages -------
library(brms)
library(loo)
library(ggplot2)
### set paths ------
working_dir = "/data/pt_02020/wd/Behav/memory/"
model_fits_dir = "/data/pt_02020/wd/Behav/memory/model_fits/"
model_comparisons_dir = "/data/pt_02020/Analysis/analysis_r_memory/Model_comparisons/"

### load dataframe ---------
recognition_performance <- read.table(paste0(working_dir, "recognition_performance_clean_with_ANT.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

discrimination_performance <- read.table(paste0(working_dir, "discrimination_performance_clean_with_ANT.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

memory_task <- readRDS( paste0(working_dir, "memory_task_output_correctedSDT_with_curated_wanting_after_exclusion_with_anthropometrics_and_image_characteristica_and_neoffi_and_ANT.RDS"))


### factorize -----
recognition_performance$Gender <- factor(recognition_performance$Gender)
discrimination_performance$Gender <- factor(discrimination_performance$Gender)
memory_task$Gender <- factor(memory_task$Gender, levels=c("F", "M"))
memory_task$response_correct <- factor(memory_task$response_correct)

# ### Define full models -----
priors = set_prior("normal(0,10)", class = "b")

full.dprime.ANT = brm(dprime ~ alerting + orienting + executive.attention + alerting:Gender + orienting:Gender + executive.attention:Gender + Gender + Age + (1 + (alerting + orienting + executive.attention | Subject)),
                         data=recognition_performance,
                         chains = 2, cores=2, iter=4000, warmup=1000,
                         prior = priors,
                         sample_prior = TRUE,
                         save_pars = save_pars(all = TRUE),
                         control = list(adapt_delta = 0.995, max_treedepth = 15))
full.dprime.ANT <- add_criterion(full.dprime.ANT,  "loo", moment_match = T, reloo=T)
saveRDS(full.dprime.ANT, paste0(model_fits_dir, "ANT/full.dprime.ANT.RDS"))

full.ldi.ANT = brm(LDI ~ alerting + orienting + executive.attention + alerting:Gender + orienting:Gender + executive.attention:Gender + Gender + Age + (1 + (alerting + orienting + executive.attention | Subject)),
                         data=discrimination_performance,
                         chains = 2, cores=2, iter=4000, warmup=1000,
                         prior = priors,
                         sample_prior = TRUE,
                         save_pars = save_pars(all = TRUE),
                         control = list(adapt_delta = 0.995, max_treedepth = 15))
full.ldi.ANT <- add_criterion(full.ldi.ANT,  "loo", moment_match = T, reloo=T)
saveRDS(full.ldi.ANT, paste0(model_fits_dir, "ANT/full.ldi.ANT.RDS"))

full.single_performance.ANT <- brm(response_correct ~  alerting + orienting + executive.attention + alerting:Gender + orienting:Gender + executive.attention:Gender + Gender + Age + (1 + (alerting + orienting + executive.attention | Subject) + (1 | Image)),
                                          data=memory_task,
                                          family = bernoulli(link = "logit"),
                                          chains = 2, cores=2, iter=4000, warmup=1000,
                                          sample_prior = TRUE,
                                          save_pars = save_pars(all = TRUE),
                                          control = list(adapt_delta = 0.995, max_treedepth = 15))
full.single_performance.ANT <- add_criterion(full.single_performance.ANT,  "loo", moment_match = T, reloo=T)
saveRDS(full.single_performance.ANT, paste0(model_fits_dir, "ANT/full.single_performance.ANT.RDS"))

### read models --------
full.dprime.ANT <- readRDS(paste0(model_fits_dir, "ANT/full.dprime.ANT.RDS"))
full.ldi.ANT <- readRDS(paste0(model_fits_dir, "ANT/full.ldi.ANT.RDS"))
full.single_performance.ANT <- readRDS(paste0(model_fits_dir, "ANT/full.single_performance.ANT.RDS"))


### plot model results -----

plot_model(full.dprime.ANT, 
           vline.color = "black", 
           show.values = TRUE, 
           value.offset = .3,
           value.size = 5,
           bpe = "mean",
           bpe.style = "dot",
           bpe.color = "white",
           dot.size = 2,
           line.size = 1,
           ci.style = "whisker",
           prob.inner = .5,
           prob.outer = .95,
           auto.label=F,
           title = "d' ~ Attention Network performance",
           axis.labels=rev(c("Alerting", "Orienting", "Executive Attention", "Gender (M > F)", "Age", 
                             "Alerting * Gender (M > F)", "Orienting * Gender (M > F)", "Executive Attention * Gender (M > F)")))

ggsave("Figures/ANT/dprime_ANT_full_model_results.png",
       plot = last_plot(),
       device = "png",
       width = 18,
       height = 22,
       units = c("cm"),
       dpi = 300,
       bg = "white") 

plot_model(full.ldi.ANT, 
           vline.color = "black", 
           show.values = TRUE, 
           value.offset = .3,
           value.size = 5,
           bpe = "mean",
           bpe.style = "dot",
           bpe.color = "white",
           dot.size = 2,
           line.size = 1,
           ci.style = "whisker",
           prob.inner = .5,
           prob.outer = .95,
           auto.label=F,
           title = "LDI ~ Attention Network performance",
           axis.labels=rev(c("Alerting", "Orienting", "Executive Attention", "Gender (M > F)", "Age", 
                             "Alerting * Gender (M > F)", "Orienting * Gender (M > F)", "Executive Attention * Gender (M > F)")))

ggsave("Figures/ANT/ldi_ANT_full_model_results.png",
       plot = last_plot(),
       device = "png",
       width = 18,
       height = 22,
       units = c("cm"),
       dpi = 300,
       bg = "white") 

plot_model(full.single_performance.ANT, 
           vline.color = "black", 
           show.values = TRUE, 
           value.offset = .3,
           value.size = 5,
           bpe = "mean",
           bpe.style = "dot",
           bpe.color = "white",
           dot.size = 2,
           line.size = 1,
           ci.style = "whisker",
           prob.inner = .5,
           prob.outer = .95,
           auto.label=F,
           title = "Response accuracy ~ Attention Network performance",
           axis.labels=rev(c("Alerting", "Orienting", "Executive Attention", "Gender (M > F)", "Age", 
                             "Alerting * Gender (M > F)", "Orienting * Gender (M > F)", "Executive Attention * Gender (M > F)")))

ggsave("Figures/ANT/single_performance_ANT_full_model_results.png",
       plot = last_plot(),
       device = "png",
       width = 18,
       height = 22,
       units = c("cm"),
       dpi = 300,
       bg = "white") 



