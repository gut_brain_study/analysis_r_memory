plot_prior_post_dist <- function (model, colors = c("red", "blue"), zero.line = "red", axis.lim=NULL, facets, axis.labels=c("Distribution", "density"), 
          ...) 
{
  if (missing(facets)) 
    facets <- TRUE
  alpha <- 0.3
  scale <- 0.9
  if (inherits(model, "brmsfit")) {
    if (!requireNamespace("brms", quietly = TRUE)) 
      stop("Package `brms` needs to be loaded first!", 
           call. = F)
    fixed_effects <- paste0("b_", rownames(summary(model)$fixed))[-1]
    d2 <- brms::prior_draws(model, variable=fixed_effects)
    if (is.null(d2)) 
      stop("No prior-samples found. Please use option `sample_prior = TRUE` when fitting the model.", 
           call. = FALSE)
    d1 <- brms::posterior_samples(model)
    d1 <- subset(d1, select = fixed_effects)
    }
  else if (inherits(model, c("stanreg", "stanfit"))) {
    if (!requireNamespace("rstanarm", quietly = TRUE)) 
      stop("Package `rstanarm` needs to be loaded first!", 
           call. = F)
    prior <- suppressWarnings(stats::update(model, prior_PD = TRUE, 
                                            refresh = -1, iter = 2000, chains = 2))
    d1 <- as.data.frame(model)
    d2 <- as.data.frame(prior)
    if (obj_has_name(d1, "(Intercept)")) 
      d1 <- dplyr::select(d1, -.data$`(Intercept)`)
    if (obj_has_name(d2, "(Intercept)")) 
      d2 <- dplyr::select(d2, -.data$`(Intercept)`)
    if (obj_has_name(d1, "sigma")) 
      d1 <- dplyr::select(d1, -.data$sigma)
    if (obj_has_name(d2, "sigma")) 
      d2 <- dplyr::select(d2, -.data$sigma)
    d1 <- dplyr::select(d1, -starts_with("b[(Intercept)", 
                                                colnames(d1)))
    d2 <- dplyr::select(d2, -starts_with("b[(Intercept)", 
                                                colnames(d2)))
    d1 <- dplyr::select(d1, -starts_with("Sigma[", 
                                                colnames(d1)))
    d2 <- dplyr::select(d2, -starts_with("Sigma[", 
                                                colnames(d2)))
  }
  d1$Sample <- "Posterior"
  d2$Sample <- "Prior"
  gather.cols <- 1:(ncol(d1) - 1)
  pp <- dplyr::bind_rows(d1, d2) %>% tidyr::gather(key = "Term", 
                                                   value = "Estimate", !!gather.cols)
  add.args <- lapply(match.call(expand.dots = F)$..., function(x) x)
  if ("alpha" %in% names(add.args)) 
    alpha <- eval(add.args[["alpha"]])
  if ("scale" %in% names(add.args)) 
    scale <- eval(add.args[["scale"]])
  if (!facets && requireNamespace("ggridges", quietly = TRUE)) {
    p <- ggplot(pp, aes_string(y = "Term", x = "Estimate", 
                               fill = "Sample")) + ggridges::geom_density_ridges2(alpha = alpha, 
                                                                                  rel_min_height = 0.005, scale = scale) + scale_fill_manual(values = colors)
  }
  else {
    p <- ggplot(pp, aes_string(x = "Estimate", fill = "Sample")) + 
      geom_density(alpha = alpha) + scale_fill_manual(values = colors)
    if (!is.null(axis.labels) && !is.null(names(axis.labels))) {
      p <- p + facet_wrap(~Term, scales = "free", labeller = labeller(.default = label_value, 
                                                                      Term = axis.labels))
    }
    else {
      p <- p + facet_wrap(~Term, scales = "free")
    }
  }
  if (!is.null(axis.lim)) 
    p <- p + scale_x_continuous(limits = axis.lim)
  p + ggplot2::geom_vline(xintercept = 0, col=zero.line)
  

}
