##### author: thieleking@cbs.mpg.de

### empty environment ------
rm(list=ls())

### load packages -------

### set paths ------
working_dir = "/data/pt_02020/wd/Behav/memory/"

### load dataframe ---------
recognition_performance <- read.table(paste0(working_dir, "recognition_performance.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

discrimination_performance <- read.table(paste0(working_dir, "discrimination_performance.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

recognition_performance_F_NF <- read.table(paste0(working_dir, "recognition_performance_F_NF.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

discrimination_performance_F_NF <- read.table(paste0(working_dir, "discrimination_performance_F_NF.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

recognition_performance_F_NF_wanting <- read.table(paste0(working_dir, "recognition_performance_F_NF_with_wanting.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

discrimination_performance_F_NF_wanting <- read.table(paste0(working_dir, "discrimination_performance_F_NF_with_wanting.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

### create dataframe with exclusions -----

###### additional post-hoc exclusion: S30, BL1 due to Diabetes: 
excluded_subjects <- data.frame(Subject=c("S04", "S09", "S25", "S30", "S34", "S36", "S38", "S42", rep("S44",2), rep("S49",4), rep("S51", 2), "S53", rep("S56", 2)),
                                Session=c(3, 4, 4, 1, 1, 4, 1, 3, 3:4, 1:4, 2:3, 3, 3:4),
                                Timepoint=NA, 
                                Reason=c("missed items n=19", "missed items n=41", "missed items n=18", "detected diabetes", "detected neurological disorder", "missed items n=20", "nausea in MRI", "missed items n=26", rep("missed items n=35", 2), "missed items n=22", "missed items n=26", "missed items n=36", "missed items n=33", "missed items n=28", "missed items n=78", "missed items n=16", "missed items n=32", "missed items n=28"))

excluded_subjects$Timepoint <- factor(excluded_subjects$Session, levels=1:4, labels=c("BL1", "FU1", "BL2", "FU2"))

for(subj in excluded_subjects$Subject){
  sessions=excluded_subjects$Session[excluded_subjects$Subject == subj]
  for(ses in sessions){
    recognition_performance <- recognition_performance[!(recognition_performance$Subject==subj & recognition_performance$Session == ses),]
    discrimination_performance <- discrimination_performance[!(discrimination_performance$Subject==subj & discrimination_performance$Session == ses),]
    recognition_performance_F_NF <- recognition_performance_F_NF[!(recognition_performance_F_NF$Subject==subj & recognition_performance_F_NF$Session == ses),]
    discrimination_performance_F_NF <- discrimination_performance_F_NF[!(discrimination_performance_F_NF$Subject==subj & discrimination_performance_F_NF$Session == ses),]
    recognition_performance_F_NF_wanting <- recognition_performance_F_NF_wanting[!(recognition_performance_F_NF_wanting$Subject==subj & recognition_performance_F_NF_wanting$Session == ses),]
    discrimination_performance_F_NF_wanting <- discrimination_performance_F_NF_wanting[!(discrimination_performance_F_NF_wanting$Subject==subj & discrimination_performance_F_NF_wanting$Session == ses),]
  }
}

### export dataframes -----
write.table(recognition_performance, paste0(working_dir, "recognition_performance_clean.csv"), quote = FALSE, sep = "\t", row.names = F)
write.table(discrimination_performance, paste0(working_dir, "discrimination_performance_clean.csv"), quote = FALSE, sep = "\t", row.names = F)
write.table(recognition_performance_F_NF, paste0(working_dir, "recognition_performance_F_NF_clean.csv"), quote = FALSE, sep = "\t", row.names = F)
write.table(discrimination_performance_F_NF, paste0(working_dir, "discrimination_performance_F_NF_clean.csv"), quote = FALSE, sep = "\t", row.names = F)
write.table(recognition_performance_F_NF_wanting, paste0(working_dir, "recognition_performance_F_NF_with_wanting_clean.csv"), quote = FALSE, sep = "\t", row.names = F)
write.table(discrimination_performance_F_NF_wanting, paste0(working_dir, "discrimination_performance_F_NF_with_wanting_clean.csv"), quote = FALSE, sep = "\t", row.names = F)

write.table(excluded_subjects, paste0(working_dir, "excluded_subjects_from_memory.csv"), quote = FALSE, sep = "\t", row.names = F)

