##### author: thieleking@cbs.mpg.de

#remove all dataframes in environment
rm(list=ls())

### load packages ------
library(tibble)

### set paths ------
input_dir = "/data/pt_02020/000_STUDY_RESULTS/"
working_dir = "/data/pt_02020/wd/Behav/memory/"


#### load memory performance --------
performance_total <- read.table(paste0(working_dir, "memory_performance_total.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

#### load and curate hunger ratings -----

hunger <- read.table(paste0(input_dir, "VAS/hunger_ratings.csv"), stringsAsFactors = F, sep = ",", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)


### deal with missing values as defined in preregistration: set single hunger rating for S04, BL2, memory and S10, BL2, memory and S17, BL1, memory as mean hunger rating for whole task

hunger$hunger_post_memory[hunger$subject == "S04" & hunger$timepoint == "BL2"] <- hunger$hunger_pre_memory[hunger$subject == "S04" & hunger$timepoint == "BL2"]
hunger$hunger_pre_memory[hunger$subject == "S10" & hunger$timepoint == "BL2"] <- hunger$hunger_post_memory[hunger$subject == "S10" & hunger$timepoint == "BL2"]
hunger$hunger_post_memory[hunger$subject == "S17" & hunger$timepoint == "BL1"] <- hunger$hunger_pre_memory[hunger$subject == "S17" & hunger$timepoint == "BL1"]


#### create empty columns for and calculate mean values ----

hunger$mean_wanting = NA
hunger$mean_memory = NA
hunger$mean_total = NA

hunger[,10] <- as.integer(hunger[,10])
hunger[,11] <- as.integer(hunger[,11])

for(i in 1:length(hunger$subject)){
  hunger$mean_wanting[i] = mean(c(hunger$hunger_pre_wanting[i], hunger$hunger_post_wanting[i]), na.rm=F)
  hunger$mean_memory[i] = mean(c(hunger$hunger_pre_memory[i], hunger$hunger_post_memory[i]), na.rm=F)
  hunger$mean_total[i] = mean(c(hunger$hunger_pre_wanting[i], hunger$hunger_post_wanting[i], hunger$hunger_pre_memory[i], hunger$hunger_post_memory[i]), na.rm=F)
}


#### create empty columns and add mean values in performance dataframe ---
performance_total <- add_column(performance_total, hunger_mean_wanting=rep(NA, 203), .before = "n_hit")
performance_total <- add_column(performance_total, hunger_mean_memory=rep(NA, 203), .before = "n_hit")
performance_total <- add_column(performance_total, hunger_mean_total=rep(NA, 203), .before = "n_hit")


for(subj in unique(performance_total$Subject)){
  for(tp in performance_total$Timepoint[performance_total$Subject == subj]){
    performance_total[performance_total$Timepoint == tp & performance_total$Subject == subj, 10:12] = hunger[hunger$timepoint == tp & hunger$subject == subj, 12:14]
  }
}


#### export new dataframes ---------

write.table(performance_total, paste0(working_dir, "memory_performance_total_with_hunger.csv"), quote = FALSE, sep = "\t", row.names = F)

