---
title: "Bayesian Linear Regression Model Comparisons regarding Brain Microstructure"
author: "thieleking@cbs.mpg.de"
date: "2023-04-25"
output:
  html_document:
    df_print: paged
  word_document: default
  pdf_document: default
---


```{r setup, include=FALSE}
library(brms)
library(loo)
working_dir = "/data/pt_02020/wd/Behav/memory/"
model_fits_dir = "/data/pt_02020/wd/Behav/memory/model_fits/"
model_comparisons_dir = "/data/pt_02020/Analysis/analysis_r_memory/Model_comparisons/"

### adjust fontsize
def.chunk.hook  <- knitr::knit_hooks$get("chunk")
knitr::knit_hooks$set(chunk = function(x, options) {
  x <- def.chunk.hook(x, options)
  paste0("\n \\", "footnotesize","\n\n", x, "\n\n \\normalsize")
})
```

## BRM comparison with loo_compare() of microstructural coherence

### Uncinate fasciculus (UF)

```{r read models UF, include=FALSE}
full.nQA_UF <- readRDS(paste0(model_fits_dir, "Microstructure/full.nQA_UF.RDS"))
full.nQA_UF.BMI <- readRDS(paste0(model_fits_dir, "Microstructure/full.nQA_UF.BMI.RDS"))
full.nQA_UF.WHR_stand <- readRDS(paste0(model_fits_dir, "Microstructure/full.nQA_UF.WHR_stand.RDS"))
full.nQA_UF.Waist.Height.Ratio_stand <- readRDS(paste0(model_fits_dir, "Microstructure/full.nQA_UF.Waist.Height.Ratio_stand.RDS"))
full.nQA_UF.FM_stand <- readRDS(paste0(model_fits_dir, "Microstructure/full.nQA_UF.FM_stand.RDS"))

null.nQA_UF.noTPinteraction <- readRDS(paste0(model_fits_dir, "Microstructure/null.nQA_UF.noTPinteraction.RDS"))
null.nQA_UF.nogenderinteraction <- readRDS(paste0(model_fits_dir, "Microstructure/null.nQA_UF.nogenderinteraction.RDS"))
null.nQA_UF.BMI.nogenderinteraction <- readRDS(paste0(model_fits_dir, "Microstructure/null.nQA_UF.BMI.nogenderinteraction.RDS"))

null.nQA_UF.noage <- readRDS(paste0(model_fits_dir, "Microstructure/null.nQA_UF.noage.RDS"))
null.nQA_UF.nogender <- readRDS(paste0(model_fits_dir, "Microstructure/null.nQA_UF.nogender.RDS"))
null.nQA_UF.BMI <- readRDS(paste0(model_fits_dir, "Microstructure/null.nQA_UF.BMI.RDS"))
null.nQA_UF.WHR_stand <- readRDS(paste0(model_fits_dir, "Microstructure/null.nQA_UF.WHR_stand.RDS"))
null.nQA_UF.Waist.Height.Ratio_stand <- readRDS(paste0(model_fits_dir, "Microstructure/null.nQA_UF.Waist.Height.Ratio_stand.RDS"))
null.nQA_UF.FM_stand <- readRDS(paste0(model_fits_dir, "Microstructure/null.nQA_UF.FM_stand.RDS"))
```
#### Age, Gender and Intervention 

```{r compare models UF, echo=FALSE}
comparison.nQA_UF <- loo_compare(full.nQA_UF,
                                 null.nQA_UF.noTPinteraction,
                                 null.nQA_UF.nogenderinteraction,
                                 null.nQA_UF.noage,
                                 null.nQA_UF.nogender,
                                 criterion = "loo")

print(comparison.nQA_UF, simplify=T)
```

#### Anthropometrics

```{r compare models UF anthro, echo=FALSE}
comparison.nQA_UF.BMI <- loo_compare(full.nQA_UF.BMI,
                                 null.nQA_UF.BMI.nogenderinteraction,
                                 null.nQA_UF.BMI,
                                 criterion = "loo")

print(comparison.nQA_UF.BMI, simplify=T)

comparison.nQA_UF.WHR_stand <- loo_compare(full.nQA_UF.WHR_stand,
                                 null.nQA_UF.WHR_stand,
                                 criterion = "loo")

print(comparison.nQA_UF.WHR_stand, simplify=T)

comparison.nQA_UF.Waist.Height.Ratio_stand <- loo_compare(full.nQA_UF.Waist.Height.Ratio_stand,
                                 null.nQA_UF.Waist.Height.Ratio_stand,
                                 criterion = "loo")

print(comparison.nQA_UF.Waist.Height.Ratio_stand, simplify=T)

comparison.nQA_UF.FM_stand <- loo_compare(full.nQA_UF.FM_stand,
                                 null.nQA_UF.FM_stand,
                                 criterion = "loo")

print(comparison.nQA_UF.FM_stand, simplify=T)
```

### Whole Brain

```{r read models wb, include=FALSE}
full.nQA_wb <- readRDS(paste0(model_fits_dir, "Microstructure/full.nQA_wb.RDS"))
full.nQA_wb.BMI <- readRDS(paste0(model_fits_dir, "Microstructure/full.nQA_wb.BMI.RDS"))
full.nQA_wb.WHR_stand <- readRDS(paste0(model_fits_dir, "Microstructure/full.nQA_wb.WHR_stand.RDS"))
full.nQA_wb.Waist.Height.Ratio_stand <- readRDS(paste0(model_fits_dir, "Microstructure/full.nQA_wb.Waist.Height.Ratio_stand.RDS"))
full.nQA_wb.FM_stand <- readRDS(paste0(model_fits_dir, "Microstructure/full.nQA_wb.FM_stand.RDS"))

null.nQA_wb.noTPinteraction <- readRDS(paste0(model_fits_dir, "Microstructure/null.nQA_wb.noTPinteraction.RDS"))
null.nQA_wb.nogenderinteraction <- readRDS(paste0(model_fits_dir, "Microstructure/null.nQA_wb.nogenderinteraction.RDS"))
null.nQA_wb.BMI.nogenderinteraction <- readRDS(paste0(model_fits_dir, "Microstructure/null.nQA_wb.BMI.nogenderinteraction.RDS"))

null.nQA_wb.noage <- readRDS(paste0(model_fits_dir, "Microstructure/null.nQA_wb.noage.RDS"))
null.nQA_wb.nogender <- readRDS(paste0(model_fits_dir, "Microstructure/null.nQA_wb.nogender.RDS"))
null.nQA_wb.BMI <- readRDS(paste0(model_fits_dir, "Microstructure/null.nQA_wb.BMI.RDS"))
null.nQA_wb.WHR_stand <- readRDS(paste0(model_fits_dir, "Microstructure/null.nQA_wb.WHR_stand.RDS"))
null.nQA_wb.Waist.Height.Ratio_stand <- readRDS(paste0(model_fits_dir, "Microstructure/null.nQA_wb.Waist.Height.Ratio_stand.RDS"))
null.nQA_wb.FM_stand <- readRDS(paste0(model_fits_dir, "Microstructure/null.nQA_wb.FM_stand.RDS"))
```
#### Age, Gender and Intervention 

```{r compare models wb, echo=FALSE}
comparison.nQA_wb <- loo_compare(full.nQA_wb,
                                 null.nQA_wb.noTPinteraction,
                                 null.nQA_wb.nogenderinteraction,
                                 null.nQA_wb.noage,
                                 null.nQA_wb.nogender,
                                 criterion = "loo")

print(comparison.nQA_wb, simplify=T)
```

#### Anthropometrics

```{r compare models wb anthro, echo=FALSE}
comparison.nQA_wb.BMI <- loo_compare(full.nQA_wb.BMI,
                                 null.nQA_wb.BMI.nogenderinteraction,
                                 null.nQA_wb.BMI,
                                 criterion = "loo")

print(comparison.nQA_wb.BMI, simplify=T)

comparison.nQA_wb.WHR_stand <- loo_compare(full.nQA_wb.WHR_stand,
                                 null.nQA_wb.WHR_stand,
                                 criterion = "loo")

print(comparison.nQA_wb.WHR_stand, simplify=T)

comparison.nQA_wb.Waist.Height.Ratio_stand <- loo_compare(full.nQA_wb.Waist.Height.Ratio_stand,
                                 null.nQA_wb.Waist.Height.Ratio_stand,
                                 criterion = "loo")

print(comparison.nQA_wb.Waist.Height.Ratio_stand, simplify=T)

comparison.nQA_wb.FM_stand <- loo_compare(full.nQA_wb.FM_stand,
                                 null.nQA_wb.FM_stand,
                                 criterion = "loo")

print(comparison.nQA_wb.FM_stand, simplify=T)
```
