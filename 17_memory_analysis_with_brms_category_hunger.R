##### author: thieleking@cbs.mpg.de

### empty environment ------
rm(list=ls())

### load packages -------
library(brms)
library(loo)

### set paths ------
working_dir = "/data/pt_02020/wd/Behav/memory/"
model_fits_dir = "/data/pt_02020/wd/Behav/memory/model_fits/"
model_comparisons_dir = "/data/pt_02020/Analysis/analysis_r_memory/Model_comparisons/"

### load dataframe ---------
recognition_performance_F_NF <- read.table(paste0(working_dir, "recognition_performance_F_NF_clean.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

discrimination_performance_F_NF <- read.table(paste0(working_dir, "discrimination_performance_F_NF_clean.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

### factorize -----
recognition_performance_F_NF$Timepoint <- factor(recognition_performance_F_NF$Timepoint)
discrimination_performance_F_NF$Timepoint <- factor(discrimination_performance_F_NF$Timepoint)
recognition_performance_F_NF$Timepoint_short <- factor(recognition_performance_F_NF$Timepoint_short)
discrimination_performance_F_NF$Timepoint_short <- factor(discrimination_performance_F_NF$Timepoint_short)
recognition_performance_F_NF$Set <- factor(recognition_performance_F_NF$Set)
discrimination_performance_F_NF$Set <- factor(discrimination_performance_F_NF$Set)
recognition_performance_F_NF$Intervention <- factor(recognition_performance_F_NF$Intervention)
discrimination_performance_F_NF$Intervention <- factor(discrimination_performance_F_NF$Intervention)
recognition_performance_F_NF$Time_of_day <- factor(recognition_performance_F_NF$Time_of_day)
discrimination_performance_F_NF$Time_of_day <- factor(discrimination_performance_F_NF$Time_of_day)
recognition_performance_F_NF$Gender <- factor(recognition_performance_F_NF$Gender)
discrimination_performance_F_NF$Gender <- factor(discrimination_performance_F_NF$Gender)
recognition_performance_F_NF$Category <- factor(recognition_performance_F_NF$Category, levels=c("NF", "F"))
discrimination_performance_F_NF$Category <- factor(discrimination_performance_F_NF$Category, levels=c("NF", "F"))

### Define full models -----
priors = set_prior("normal(0,10)", class = "b")

full.dprime.category.hunger.allrandomeffects = brm(dprime ~ Category + hunger_mean_total + Category:hunger_mean_total + Gender + Age + Timepoint_short + Intervention + Timepoint_short:Intervention + (1 + (Category + hunger_mean_total + Category:hunger_mean_total | Subject) + (Category | Set) ),
                                                   data=recognition_performance_F_NF,
                                                   chains = 2, cores=2, iter=4000, warmup=1000,
                                                   prior = priors,
                                                   sample_prior = TRUE,
                                                   save_pars = save_pars(all = TRUE),
                                                   control = list(adapt_delta = 0.995, max_treedepth  = 15))

full.ldi.category.hunger.allrandomeffects = brm(LDI ~ Category + hunger_mean_total + Category:hunger_mean_total + Gender + Age + Timepoint_short + Intervention + Timepoint_short:Intervention + (1 + (Category + hunger_mean_total + Category:hunger_mean_total | Subject) + (Category | Set) ),
                                                data=discrimination_performance_F_NF,
                                                chains = 2, cores=2, iter=4000, warmup=1000,
                                                prior = priors,
                                                sample_prior = TRUE,
                                                save_pars = save_pars(all = TRUE),
                                                control = list(adapt_delta = 0.999, max_treedepth  = 15))

### Define models with less random effects ----
##### no random interaction --------
full.dprime.category.hunger.norandominteraction =
  brm(dprime ~ Category + hunger_mean_total + Category:hunger_mean_total + Gender + Age + Timepoint_short + Intervention + Timepoint_short:Intervention + (1 + (Category + hunger_mean_total | Subject) + (Category | Set) ),
      data=recognition_performance_F_NF,
      chains = 2, cores=2, iter=4000, warmup=1000,
      prior = priors,
      sample_prior = TRUE,
      save_pars = save_pars(all = TRUE),
      control = list(adapt_delta = 0.995, max_treedepth  = 15))

full.ldi.category.hunger.norandominteraction =
  brm(LDI ~ Category + hunger_mean_total + Category:hunger_mean_total + Gender + Age + Timepoint_short + Intervention + Timepoint_short:Intervention + (1 + (Category + hunger_mean_total  | Subject) + (Category | Set) ),
      data=discrimination_performance_F_NF,
      chains = 2, cores=2, iter=4000, warmup=1000,
      prior = priors,
      sample_prior = TRUE,
      save_pars = save_pars(all = TRUE),
      control = list(adapt_delta = 0.995, max_treedepth  = 15))

##### no random slope hunger --------
full.dprime.category.hunger.norandomslopehunger =
  brm(dprime ~ Category + hunger_mean_total + Category:hunger_mean_total + Gender + Age + Timepoint_short + Intervention + Timepoint_short:Intervention + (1 + (Category | Subject) + (Category | Set) ),
      data=recognition_performance_F_NF,
      chains = 2, cores=2, iter=4000, warmup=1000,
      prior = priors,
      sample_prior = TRUE,
      save_pars = save_pars(all = TRUE),
      control = list(adapt_delta = 0.995, max_treedepth  = 15))

full.ldi.category.hunger.norandomslopehunger =
  brm(LDI ~ Category + hunger_mean_total + Category:hunger_mean_total + Gender + Age + Timepoint_short + Intervention + Timepoint_short:Intervention + (1 + (Category | Subject) + (Category | Set) ),
      data=discrimination_performance_F_NF,
      chains = 2, cores=2, iter=4000, warmup=1000,
      prior = priors,
      sample_prior = TRUE,
      save_pars = save_pars(all = TRUE),
      control = list(adapt_delta = 0.995, max_treedepth  = 15))

##### no random slope category --------
full.dprime.category.hunger.norandomslopecategory =
  brm(dprime ~ Category + hunger_mean_total + Category:hunger_mean_total + Gender + Age + Timepoint_short + Intervention + Timepoint_short:Intervention + (1 + (1 | Subject) + (Category | Set) ),
      data=recognition_performance_F_NF,
      chains = 2, cores=2, iter=4000, warmup=1000,
      prior = priors,
      sample_prior = TRUE,
      save_pars = save_pars(all = TRUE),
      control = list(adapt_delta = 0.995, max_treedepth  = 15))

full.ldi.category.hunger.norandomslopecategory =
  brm(LDI ~ Category + hunger_mean_total + Category:hunger_mean_total + Gender + Age + Timepoint_short + Intervention + Timepoint_short:Intervention + (1 + (1 | Subject) + (Category | Set) ),
      data=discrimination_performance_F_NF,
      chains = 2, cores=2, iter=4000, warmup=1000,
      prior = priors,
      sample_prior = TRUE,
      save_pars = save_pars(all = TRUE),
      control = list(adapt_delta = 0.995, max_treedepth  = 15))

##### no random slopes --------
full.dprime.category.hunger.norandomslopes =
  brm(dprime ~ Category + hunger_mean_total + Category:hunger_mean_total + Gender + Age + Timepoint_short + Intervention + Timepoint_short:Intervention + (1 + (1 | Subject) + (1 | Set) ),
      data=recognition_performance_F_NF,
      chains = 2, cores=2, iter=4000, warmup=1000,
      prior = priors,
      sample_prior = TRUE,
      save_pars = save_pars(all = TRUE),
      control = list(adapt_delta = 0.995, max_treedepth  = 15))

full.ldi.category.hunger.norandomslopes =
  brm(LDI ~ Category + hunger_mean_total + Category:hunger_mean_total + Gender + Age + Timepoint_short + Intervention + Timepoint_short:Intervention + (1 + (1 | Subject) + (1 | Set) ),
      data=discrimination_performance_F_NF,
      chains = 2, cores=2, iter=4000, warmup=1000,
      prior = priors,
      sample_prior = TRUE,
      save_pars = save_pars(all = TRUE),
      control = list(adapt_delta = 0.995, max_treedepth  = 15))

### Test for random effects ----
##### add criterion="loo" to full models ------
full.dprime.category.hunger.allrandomeffects <- add_criterion(full.dprime.category.hunger.allrandomeffects, "loo", moment_match = T, reloo=T)
full.ldi.category.hunger.allrandomeffects <- add_criterion(full.ldi.category.hunger.allrandomeffects, "loo", moment_match = T, reloo=T)

##### add criterion="loo" to less random effects models ------
full.dprime.category.hunger.norandominteraction <- add_criterion(full.dprime.category.hunger.norandominteraction, "loo", moment_match = T, reloo=T)
full.ldi.category.hunger.norandominteraction <- add_criterion(full.ldi.category.hunger.norandominteraction, "loo", moment_match = T, reloo=T)
full.dprime.category.hunger.norandomslopehunger <- add_criterion(full.dprime.category.hunger.norandomslopehunger, "loo", moment_match = T, reloo=T)
full.ldi.category.hunger.norandomslopehunger <- add_criterion(full.ldi.category.hunger.norandomslopehunger, "loo", moment_match = T, reloo=T)
full.dprime.category.hunger.norandomslopecategory <- add_criterion(full.dprime.category.hunger.norandomslopecategory, "loo", moment_match = T, reloo=T)
full.ldi.category.hunger.norandomslopecategory <- add_criterion(full.ldi.category.hunger.norandomslopecategory, "loo", moment_match = T, reloo=T)
full.dprime.category.hunger.norandomslopes <- add_criterion(full.dprime.category.hunger.norandomslopes, "loo", moment_match = T, reloo=T)
full.ldi.category.hunger.norandomslopes <- add_criterion(full.ldi.category.hunger.norandomslopes, "loo", moment_match = T, reloo=T)

### save models ----
saveRDS(full.dprime.category.hunger.allrandomeffects, paste0(model_fits_dir, "Hunger_TPxIntervention/full.dprime.category.hunger.allrandomeffects.RDS"))
saveRDS(full.dprime.category.hunger.norandominteraction, paste0(model_fits_dir, "Hunger_TPxIntervention/full.dprime.category.hunger.norandominteraction.RDS"))
saveRDS(full.dprime.category.hunger.norandomslopehunger, paste0(model_fits_dir, "Hunger_TPxIntervention/full.dprime.category.hunger.norandomslopehunger.RDS"))
saveRDS(full.dprime.category.hunger.norandomslopecategory, paste0(model_fits_dir, "Hunger_TPxIntervention/full.dprime.category.hunger.norandomslopecategory.RDS"))
saveRDS(full.dprime.category.hunger.norandomslopes, paste0(model_fits_dir, "Hunger_TPxIntervention/full.dprime.category.hunger.norandomslopes.RDS"))

saveRDS(full.ldi.category.hunger.allrandomeffects, paste0(model_fits_dir, "Hunger_TPxIntervention/full.ldi.category.hunger.allrandomeffects.RDS"))
saveRDS(full.ldi.category.hunger.norandominteraction, paste0(model_fits_dir, "Hunger_TPxIntervention/full.ldi.category.hunger.norandominteraction.RDS"))
saveRDS(full.ldi.category.hunger.norandomslopehunger, paste0(model_fits_dir, "Hunger_TPxIntervention/full.ldi.category.hunger.norandomslopehunger.RDS"))
saveRDS(full.ldi.category.hunger.norandomslopecategory, paste0(model_fits_dir, "Hunger_TPxIntervention/full.ldi.category.hunger.norandomslopecategory.RDS"))
saveRDS(full.ldi.category.hunger.norandomslopes, paste0(model_fits_dir, "Hunger_TPxIntervention/full.ldi.category.hunger.norandomslopes.RDS"))

### read models ----
full.dprime.category.hunger.allrandomeffects <- readRDS(paste0(model_fits_dir, "Hunger_TPxIntervention/full.dprime.category.hunger.allrandomeffects.RDS"))
full.dprime.category.hunger.norandominteraction <- readRDS(paste0(model_fits_dir, "Hunger_TPxIntervention/full.dprime.category.hunger.norandominteraction.RDS"))
full.dprime.category.hunger.norandomslopehunger <- readRDS(paste0(model_fits_dir, "Hunger_TPxIntervention/full.dprime.category.hunger.norandomslopehunger.RDS"))
full.dprime.category.hunger.norandomslopecategory <- readRDS(paste0(model_fits_dir, "Hunger_TPxIntervention/full.dprime.category.hunger.norandomslopecategory.RDS"))
full.dprime.category.hunger.norandomslopes <- readRDS(paste0(model_fits_dir, "Hunger_TPxIntervention/full.dprime.category.hunger.norandomslopes.RDS"))

full.ldi.category.hunger.allrandomeffects <- readRDS(paste0(model_fits_dir, "Hunger_TPxIntervention/full.ldi.category.hunger.allrandomeffects.RDS"))
full.ldi.category.hunger.norandominteraction <- readRDS(paste0(model_fits_dir, "Hunger_TPxIntervention/full.ldi.category.hunger.norandominteraction.RDS"))
full.ldi.category.hunger.norandomslopehunger <- readRDS(paste0(model_fits_dir, "Hunger_TPxIntervention/full.ldi.category.hunger.norandomslopehunger.RDS"))
full.ldi.category.hunger.norandomslopecategory <- readRDS(paste0(model_fits_dir, "Hunger_TPxIntervention/full.ldi.category.hunger.norandomslopecategory.RDS"))
full.ldi.category.hunger.norandomslopes <- readRDS(paste0(model_fits_dir, "Hunger_TPxIntervention/full.ldi.category.hunger.norandomslopes.RDS"))

### compare models regarding random effects ------
comparison.dprime.category.hunger.randomeffects <- loo_compare(full.dprime.category.hunger.allrandomeffects, full.dprime.category.hunger.norandominteraction, full.dprime.category.hunger.norandomslopehunger, full.dprime.category.hunger.norandomslopecategory, full.dprime.category.hunger.norandomslopes)

comparison.ldi.category.hunger.randomeffects <- loo_compare(full.ldi.category.hunger.allrandomeffects, full.ldi.category.hunger.norandominteraction, full.ldi.category.hunger.norandomslopehunger, full.ldi.category.hunger.norandomslopecategory, full.ldi.category.hunger.norandomslopes)

print(comparison.dprime.category.hunger.randomeffects, simplify=F)
print(comparison.ldi.category.hunger.randomeffects, simplify=F)

### Define null models -----
##### no interaction ------
null.dprime.category.hunger.norandomslopehunger =
  brm(dprime ~ Category + hunger_mean_total + Gender + Age + Timepoint_short + Intervention + Timepoint_short:Intervention + (1 + (Category | Subject) + (Category | Set) ),
      data=recognition_performance_F_NF,
      chains = 2, cores=2, iter=4000, warmup=1000,
      prior = priors,
      sample_prior = TRUE,
      save_pars = save_pars(all = TRUE),
      control = list(adapt_delta = 0.995, max_treedepth  = 15))

null.ldi.category.hunger.norandomslopehunger =
  brm(LDI ~ Category + hunger_mean_total + Gender + Age + Timepoint_short + Intervention + Timepoint_short:Intervention + (1 + (Category | Subject) + (Category | Set) ),
      data=discrimination_performance_F_NF,
      chains = 2, cores=2, iter=4000, warmup=1000,
      prior = priors,
      sample_prior = TRUE,
      save_pars = save_pars(all = TRUE),
      control = list(adapt_delta = 0.999, max_treedepth  = 15))

##### no hunger -----------
null.dprime.category.norandomslopehunger =
  brm(dprime ~ Category + Gender + Age + Timepoint_short + Intervention + Timepoint_short:Intervention + (1 + (Category | Subject) + (Category | Set) ),
      data=recognition_performance_F_NF,
      chains = 2, cores=2, iter=4000, warmup=1000,
      prior = priors,
      sample_prior = TRUE,
      save_pars = save_pars(all = TRUE),
      control = list(adapt_delta = 0.995, max_treedepth  = 15))

null.ldi.category.norandomslopehunger =
  brm(LDI ~ Category + Gender + Age + Timepoint_short + Intervention + Timepoint_short:Intervention + (1 + (Category | Subject) + (Category | Set) ),
      data=discrimination_performance_F_NF,
      chains = 2, cores=2, iter=4000, warmup=1000,
      prior = priors,
      sample_prior = TRUE,
      save_pars = save_pars(all = TRUE),
      control = list(adapt_delta = 0.995, max_treedepth  = 15))

##### no hunger or category -----------
null.dprime.norandomslopehunger =
  brm(dprime ~ Gender + Age + Timepoint_short + Intervention + Timepoint_short:Intervention + (1 + (Category | Subject) + (Category | Set) ),
      data=recognition_performance_F_NF,
      chains = 2, cores=2, iter=4000, warmup=1000,
      prior = priors,
      sample_prior = TRUE,
      save_pars = save_pars(all = TRUE),
      control = list(adapt_delta = 0.995, max_treedepth  = 15))

null.ldi.norandomslopehunger =
  brm(LDI ~ Gender + Age + Timepoint_short + Intervention + Timepoint_short:Intervention + (1 + (Category | Subject) + (Category | Set) ),
      data=discrimination_performance_F_NF,
      chains = 2, cores=2, iter=4000, warmup=1000,
      prior = priors,
      sample_prior = TRUE,
      save_pars = save_pars(all = TRUE),
      control = list(adapt_delta = 0.995, max_treedepth  = 15))

##### add criterion="loo" to null models ------
null.dprime.category.hunger.norandomslopehunger <- add_criterion(null.dprime.category.hunger.norandomslopehunger, "loo", moment_match = T, reloo=T)
null.ldi.category.hunger.norandomslopehunger <- add_criterion(null.ldi.category.hunger.norandomslopehunger, "loo", moment_match = T, reloo=T)
null.dprime.category.norandomslopehunger <- add_criterion(null.dprime.category.norandomslopehunger, "loo", moment_match = T, reloo=T)
null.ldi.category.norandomslopehunger <- add_criterion(null.ldi.category.norandomslopehunger, "loo", moment_match = T, reloo=T)
null.dprime.norandomslopehunger <- add_criterion(null.dprime.norandomslopehunger, "loo", moment_match = T, reloo=T)
null.ldi.norandomslopehunger <- add_criterion(null.ldi.norandomslopehunger, "loo", moment_match = T, reloo=T)

### save models ----
saveRDS(null.dprime.category.hunger.norandomslopehunger, paste0(model_fits_dir, "Hunger_TPxIntervention/null.dprime.category.hunger.norandomslopehunger.RDS"))
saveRDS(null.ldi.category.hunger.norandomslopehunger, paste0(model_fits_dir, "Hunger_TPxIntervention/null.ldi.category.hunger.norandomslopehunger.RDS"))
saveRDS(null.dprime.category.norandomslopehunger, paste0(model_fits_dir, "Hunger_TPxIntervention/null.dprime.category.norandomslopehunger.RDS"))
saveRDS(null.ldi.category.norandomslopehunger, paste0(model_fits_dir, "Hunger_TPxIntervention/null.ldi.category.norandomslopehunger.RDS"))
saveRDS(null.dprime.norandomslopehunger, paste0(model_fits_dir, "Hunger_TPxIntervention/null.dprime.norandomslopehunger.RDS"))
saveRDS(null.ldi.norandomslopehunger, paste0(model_fits_dir, "Hunger_TPxIntervention/null.ldi.norandomslopehunger.RDS"))

### read models --------
full.dprime.category.hunger.norandomslopehunger <- readRDS(paste0(model_fits_dir, "Hunger_TPxIntervention/full.dprime.category.hunger.norandomslopehunger.RDS"))
full.ldi.category.hunger.norandomslopehunger <- readRDS(paste0(model_fits_dir, "Hunger_TPxIntervention/full.ldi.category.hunger.norandomslopehunger.RDS"))
null.dprime.category.hunger.norandomslopehunger <- readRDS(paste0(model_fits_dir, "Hunger_TPxIntervention/null.dprime.category.hunger.norandomslopehunger.RDS"))
null.ldi.category.hunger.norandomslopehunger <- readRDS(paste0(model_fits_dir, "Hunger_TPxIntervention/null.ldi.category.hunger.norandomslopehunger.RDS"))
null.dprime.category.norandomslopehunger <- readRDS(paste0(model_fits_dir, "Hunger_TPxIntervention/null.dprime.category.norandomslopehunger.RDS"))
null.ldi.category.norandomslopehunger <- readRDS(paste0(model_fits_dir, "Hunger_TPxIntervention/null.ldi.category.norandomslopehunger.RDS"))
null.dprime.norandomslopehunger <- readRDS(paste0(model_fits_dir, "Hunger_TPxIntervention/null.dprime.norandomslopehunger.RDS"))
null.ldi.norandomslopehunger <- readRDS(paste0(model_fits_dir, "Hunger_TPxIntervention/null.ldi.norandomslopehunger.RDS"))

### compare models regarding fixed effects ------
comparison.dprime.category.hunger <- loo_compare(full.dprime.category.hunger.norandomslopehunger, null.dprime.category.hunger.norandomslopehunger, null.dprime.category.norandomslopehunger, null.dprime.norandomslopehunger, criterion = "loo")
comparison.ldi.category.hunger <- loo_compare(full.ldi.category.hunger.norandomslopehunger, null.ldi.category.hunger.norandomslopehunger, null.ldi.category.norandomslopehunger, null.ldi.norandomslopehunger, criterion = "loo")

print(comparison.dprime.category.hunger, simplify=F)
print(comparison.ldi.category.hunger, simplify=F)

sjPlot::tab_model(full.dprime.category.hunger.norandomslopehunger, 
                  null.dprime.category.hunger.norandomslopehunger, 
                  null.dprime.category.norandomslopehunger, 
                  #null.dprime.norandomslopehunger, 
                  collapse.ci = TRUE, ### get CIs below estimate
                  pred.labels = c("Intercept", "Category (F > NF)", "Subj Hunger Level", 
                                  "Gender (M > F)", "Age", "Timepoint (FU > BL)", "Intervention (Fiber > Placebo)", 
                                  "Category (F > NF) * Subj Hunger Level", 
                                  "Timepoint * Intervention"),
                  dv.labels = c("full model", "null model 1", "null model 2"), ### rename models
                  string.pred = c("Predictors for d'"), ### rename column with predictors
                  file = paste0(model_comparisons_dir, "comparison.dprime.category.hunger.TPxIntervention.html"))

sjPlot::tab_model(full.ldi.category.hunger.norandomslopehunger, 
                  null.ldi.category.hunger.norandomslopehunger, 
                  null.ldi.category.norandomslopehunger, 
                  #null.ldi.norandomslopehunger, 
                  collapse.ci = TRUE, ### get CIs below estimate
                  pred.labels = c("Intercept", "Category (F > NF)", "Subj Hunger Level", 
                                  "Gender (M > F)", "Age", "Timepoint (FU > BL)", "Intervention (Fiber > Placebo)", 
                                  "Category (F > NF) * Subj Hunger Level", 
                                  "Timepoint * Intervention"),
                  dv.labels = c("full model", "null model 1", "null model 2"), ### rename models
                  string.pred = c("Predictors for LDI"), ### rename column with predictors
                  file = paste0(model_comparisons_dir, "comparison.ldi.category.hunger.TPxIntervention.html"))

