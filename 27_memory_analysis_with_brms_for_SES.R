##### author: thieleking@cbs.mpg.de

### empty environment ------
rm(list=ls())

### load packages -------
library(brms)
library(loo)

### set paths ------
working_dir = "/data/pt_02020/wd/Behav/memory/"
model_fits_dir = "/data/pt_02020/wd/Behav/memory/model_fits/"
model_comparisons_dir = "/data/pt_02020/Analysis/analysis_r_memory/Model_comparisons/"

### load dataframe ---------
recognition_performance <- read.table(paste0(working_dir, "recognition_performance_SES_clean.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

discrimination_performance <- read.table(paste0(working_dir, "discrimination_performance_SES_clean.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

### factorize -----
recognition_performance$Timepoint <- factor(recognition_performance$Timepoint)
discrimination_performance$Timepoint <- factor(discrimination_performance$Timepoint)
recognition_performance$Set <- factor(recognition_performance$Set)
discrimination_performance$Set <- factor(discrimination_performance$Set)
recognition_performance$Intervention <- factor(recognition_performance$Intervention)
discrimination_performance$Intervention <- factor(discrimination_performance$Intervention)
recognition_performance$Time_of_day <- factor(recognition_performance$Time_of_day)
discrimination_performance$Time_of_day <- factor(discrimination_performance$Time_of_day)
recognition_performance$Gender <- factor(recognition_performance$Gender)
discrimination_performance$Gender <- factor(discrimination_performance$Gender)

### Define full models -----
priors = set_prior("normal(0,10)", class = "b")

full.dprime.SES_sum = brm(dprime ~ SES_sum + Gender + Age + (1 + (SES_sum | Subject)), 
                         data=recognition_performance, 
                         chains = 2, cores=2, iter=4000, warmup=1000,
                         prior = priors,
                         sample_prior = TRUE,
                         save_pars = save_pars(all = TRUE),
                         control = list(adapt_delta = 0.995, max_treedepth = 15))
full.dprime.SES_sum <- add_criterion(full.dprime.SES_sum,  "loo", moment_match = T, reloo=T)
saveRDS(full.dprime.SES_sum, paste0(model_fits_dir, "SES/full.dprime.SES_sum.RDS"))

full.dprime.SES_sum.Gender = brm(dprime ~ SES_sum + Gender + Age + SES_sum:Gender + (1 + (SES_sum | Subject)), 
                         data=recognition_performance, 
                         chains = 2, cores=2, iter=4000, warmup=1000,
                         prior = priors,
                         sample_prior = TRUE,
                         save_pars = save_pars(all = TRUE),
                         control = list(adapt_delta = 0.995, max_treedepth = 15))
full.dprime.SES_sum.Gender <- add_criterion(full.dprime.SES_sum.Gender,  "loo", moment_match = T, reloo=T)
saveRDS(full.dprime.SES_sum.Gender, paste0(model_fits_dir, "SES/full.dprime.SES_sum.Gender.RDS"))

full.dprime.SES_income = brm(dprime ~ SES_income + Gender + Age + (1 + (SES_income | Subject)), 
                          data=recognition_performance, 
                          chains = 2, cores=2, iter=4000, warmup=1000,
                          prior = priors,
                          sample_prior = TRUE,
                          save_pars = save_pars(all = TRUE),
                          control = list(adapt_delta = 0.995, max_treedepth = 15))
full.dprime.SES_income <- add_criterion(full.dprime.SES_income,  "loo", moment_match = T, reloo=T)
saveRDS(full.dprime.SES_income, paste0(model_fits_dir, "SES/full.dprime.SES_income.RDS"))

full.dprime.SES_degree = brm(dprime ~ SES_degree + Gender + Age + (1 + (SES_degree | Subject)), 
                             data=recognition_performance, 
                             chains = 2, cores=2, iter=4000, warmup=1000,
                             prior = priors,
                             sample_prior = TRUE,
                             save_pars = save_pars(all = TRUE),
                             control = list(adapt_delta = 0.995, max_treedepth = 15))
full.dprime.SES_degree <- add_criterion(full.dprime.SES_degree,  "loo", moment_match = T, reloo=T)
saveRDS(full.dprime.SES_degree, paste0(model_fits_dir, "SES/full.dprime.SES_degree.RDS"))

full.dprime.SES_occupation = brm(dprime ~ SES_occupation + Gender + Age + (1 + (SES_occupation | Subject)), 
                             data=recognition_performance, 
                             chains = 2, cores=2, iter=4000, warmup=1000,
                             prior = priors,
                             sample_prior = TRUE,
                             save_pars = save_pars(all = TRUE),
                             control = list(adapt_delta = 0.995, max_treedepth = 15))
full.dprime.SES_occupation <- add_criterion(full.dprime.SES_occupation,  "loo", moment_match = T, reloo=T)
saveRDS(full.dprime.SES_occupation, paste0(model_fits_dir, "SES/full.dprime.SES_occupation.RDS"))

### too many divergent transitions
full.ldi.SES_sum = brm(LDI ~ SES_sum + Gender + Age + (1 + (SES_sum | Subject)), 
                          data=discrimination_performance, 
                          chains = 2, cores=2, iter=4000, warmup=1000,
                          prior = priors,
                          sample_prior = TRUE,
                          save_pars = save_pars(all = TRUE),
                          control = list(adapt_delta = 0.999, max_treedepth = 15))
full.ldi.SES_sum <- add_criterion(full.ldi.SES_sum,  "loo", moment_match = T, reloo=T)
saveRDS(full.ldi.SES_sum, paste0(model_fits_dir, "SES/full.ldi.SES_sum.RDS"))

full.ldi.SES_sum.Gender = brm(LDI ~ SES_sum + Gender + Age + SES_sum:Gender + (1 + (SES_sum | Subject)), 
                          data=discrimination_performance, 
                          chains = 2, cores=2, iter=4000, warmup=1000,
                          prior = priors,
                          sample_prior = TRUE,
                          save_pars = save_pars(all = TRUE),
                          control = list(adapt_delta = 0.995, max_treedepth = 15))
full.ldi.SES_sum.Gender <- add_criterion(full.ldi.SES_sum.Gender,  "loo", moment_match = T, reloo=T)
saveRDS(full.ldi.SES_sum.Gender, paste0(model_fits_dir, "SES/full.ldi.SES_sum.Gender.RDS"))

full.ldi.SES_income = brm(LDI ~ SES_income + Gender + Age + (1 + (SES_income | Subject)), 
                             data=discrimination_performance, 
                             chains = 2, cores=2, iter=4000, warmup=1000,
                             prior = priors,
                             sample_prior = TRUE,
                             save_pars = save_pars(all = TRUE),
                             control = list(adapt_delta = 0.995, max_treedepth = 15))
full.ldi.SES_income <- add_criterion(full.ldi.SES_income,  "loo", moment_match = T, reloo=T)
saveRDS(full.ldi.SES_income, paste0(model_fits_dir, "SES/full.ldi.SES_income.RDS"))

full.ldi.SES_degree = brm(LDI ~ SES_degree + Gender + Age + (1 + (SES_degree | Subject)), 
                             data=discrimination_performance, 
                             chains = 2, cores=2, iter=4000, warmup=1000,
                             prior = priors,
                             sample_prior = TRUE,
                             save_pars = save_pars(all = TRUE),
                             control = list(adapt_delta = 0.995, max_treedepth = 15))
full.ldi.SES_degree <- add_criterion(full.ldi.SES_degree,  "loo", moment_match = T, reloo=T)
saveRDS(full.ldi.SES_degree, paste0(model_fits_dir, "SES/full.ldi.SES_degree.RDS"))

full.ldi.SES_occupation = brm(LDI ~ SES_occupation + Gender + Age + (1 + (SES_occupation | Subject)), 
                                 data=discrimination_performance, 
                                 chains = 2, cores=2, iter=4000, warmup=1000,
                                 prior = priors,
                                 sample_prior = TRUE,
                                 save_pars = save_pars(all = TRUE),
                                 control = list(adapt_delta = 0.995, max_treedepth = 15))
full.ldi.SES_occupation <- add_criterion(full.ldi.SES_occupation,  "loo", moment_match = T, reloo=T)
saveRDS(full.ldi.SES_occupation, paste0(model_fits_dir, "SES/full.ldi.SES_occupation.RDS"))


### Define null models -----

null.dprime.SES_sum = brm(dprime ~ Gender + Age + (1 + (SES_sum | Subject)), 
                          data=recognition_performance, 
                          chains = 2, cores=2, iter=4000, warmup=1000,
                          prior = priors,
                          sample_prior = TRUE,
                          save_pars = save_pars(all = TRUE),
                          control = list(adapt_delta = 0.995, max_treedepth = 15))
null.dprime.SES_sum <- add_criterion(null.dprime.SES_sum,  "loo", moment_match = T, reloo=T)
saveRDS(null.dprime.SES_sum, paste0(model_fits_dir, "SES/null.dprime.SES_sum.RDS"))

null.dprime.SES_income = brm(dprime ~ Gender + Age + (1 + (SES_income | Subject)), 
                             data=recognition_performance, 
                             chains = 2, cores=2, iter=4000, warmup=1000,
                             prior = priors,
                             sample_prior = TRUE,
                             save_pars = save_pars(all = TRUE),
                             control = list(adapt_delta = 0.995, max_treedepth = 15))
null.dprime.SES_income <- add_criterion(null.dprime.SES_income,  "loo", moment_match = T, reloo=T)
saveRDS(null.dprime.SES_income, paste0(model_fits_dir, "SES/null.dprime.SES_income.RDS"))

null.dprime.SES_degree = brm(dprime ~ Gender + Age + (1 + (SES_degree | Subject)), 
                             data=recognition_performance, 
                             chains = 2, cores=2, iter=4000, warmup=1000,
                             prior = priors,
                             sample_prior = TRUE,
                             save_pars = save_pars(all = TRUE),
                             control = list(adapt_delta = 0.995, max_treedepth = 15))
null.dprime.SES_degree <- add_criterion(null.dprime.SES_degree,  "loo", moment_match = T, reloo=T)
saveRDS(null.dprime.SES_degree, paste0(model_fits_dir, "SES/null.dprime.SES_degree.RDS"))

null.dprime.SES_occupation = brm(dprime ~ Gender + Age + (1 + (SES_occupation | Subject)), 
                                 data=recognition_performance, 
                                 chains = 2, cores=2, iter=4000, warmup=1000,
                                 prior = priors,
                                 sample_prior = TRUE,
                                 save_pars = save_pars(all = TRUE),
                                 control = list(adapt_delta = 0.995, max_treedepth = 15))
null.dprime.SES_occupation <- add_criterion(null.dprime.SES_occupation,  "loo", moment_match = T, reloo=T)
saveRDS(null.dprime.SES_occupation, paste0(model_fits_dir, "SES/null.dprime.SES_occupation.RDS"))

null.ldi.SES_sum = brm(LDI ~ Gender + Age + (1 + (SES_sum | Subject)), 
                       data=discrimination_performance, 
                       chains = 2, cores=2, iter=4000, warmup=1000,
                       prior = priors,
                       sample_prior = TRUE,
                       save_pars = save_pars(all = TRUE),
                       control = list(adapt_delta = 0.995, max_treedepth = 15))
null.ldi.SES_sum <- add_criterion(null.ldi.SES_sum,  "loo", moment_match = T, reloo=T)
saveRDS(null.ldi.SES_sum, paste0(model_fits_dir, "SES/null.ldi.SES_sum.RDS"))

null.ldi.SES_income = brm(LDI ~ Gender + Age + (1 + (SES_income | Subject)), 
                          data=discrimination_performance, 
                          chains = 2, cores=2, iter=4000, warmup=1000,
                          prior = priors,
                          sample_prior = TRUE,
                          save_pars = save_pars(all = TRUE),
                          control = list(adapt_delta = 0.995, max_treedepth = 15))
null.ldi.SES_income <- add_criterion(null.ldi.SES_income,  "loo", moment_match = T, reloo=T)
saveRDS(null.ldi.SES_income, paste0(model_fits_dir, "SES/null.ldi.SES_income.RDS"))

null.ldi.SES_degree = brm(LDI ~ Gender + Age + (1 + (SES_degree | Subject)), 
                          data=discrimination_performance, 
                          chains = 2, cores=2, iter=4000, warmup=1000,
                          prior = priors,
                          sample_prior = TRUE,
                          save_pars = save_pars(all = TRUE),
                          control = list(adapt_delta = 0.995, max_treedepth = 15))
null.ldi.SES_degree <- add_criterion(null.ldi.SES_degree,  "loo", moment_match = T, reloo=T)
saveRDS(null.ldi.SES_degree, paste0(model_fits_dir, "SES/null.ldi.SES_degree.RDS"))

null.ldi.SES_occupation = brm(LDI ~ Gender + Age + (1 + (SES_occupation | Subject)), 
                              data=discrimination_performance, 
                              chains = 2, cores=2, iter=4000, warmup=1000,
                              prior = priors,
                              sample_prior = TRUE,
                              save_pars = save_pars(all = TRUE),
                              control = list(adapt_delta = 0.995, max_treedepth = 15))
null.ldi.SES_occupation <- add_criterion(null.ldi.SES_occupation,  "loo", moment_match = T, reloo=T)
saveRDS(null.ldi.SES_occupation, paste0(model_fits_dir, "SES/null.ldi.SES_occupation.RDS"))


### read models ------------
full.dprime.SES_sum <- readRDS(paste0(model_fits_dir, "SES/full.dprime.SES_sum.RDS"))
full.dprime.SES_sum.Gender <- readRDS(paste0(model_fits_dir, "SES/full.dprime.SES_sum.Gender.RDS"))
full.dprime.SES_income <- readRDS(paste0(model_fits_dir, "SES/full.dprime.SES_income.RDS"))
full.dprime.SES_degree <- readRDS(paste0(model_fits_dir, "SES/full.dprime.SES_degree.RDS"))
full.dprime.SES_occupation <- readRDS(paste0(model_fits_dir, "SES/full.dprime.SES_occupation.RDS"))

null.dprime.SES_sum <- readRDS(paste0(model_fits_dir, "SES/null.dprime.SES_sum.RDS"))
null.dprime.SES_income <- readRDS(paste0(model_fits_dir, "SES/null.dprime.SES_income.RDS"))
null.dprime.SES_degree <- readRDS(paste0(model_fits_dir, "SES/null.dprime.SES_degree.RDS"))
null.dprime.SES_occupation <- readRDS(paste0(model_fits_dir, "SES/null.dprime.SES_occupation.RDS"))

full.ldi.SES_sum <- readRDS(paste0(model_fits_dir, "SES/full.ldi.SES_sum.RDS"))
full.ldi.SES_sum.Gender <- readRDS(paste0(model_fits_dir, "SES/full.ldi.SES_sum.Gender.RDS"))
full.ldi.SES_income <- readRDS(paste0(model_fits_dir, "SES/full.ldi.SES_income.RDS"))
full.ldi.SES_degree <- readRDS(paste0(model_fits_dir, "SES/full.ldi.SES_degree.RDS"))
full.ldi.SES_occupation <- readRDS(paste0(model_fits_dir, "SES/full.ldi.SES_occupation.RDS"))

null.ldi.SES_sum <- readRDS(paste0(model_fits_dir, "SES/null.ldi.SES_sum.RDS"))
null.ldi.SES_income <- readRDS(paste0(model_fits_dir, "SES/null.ldi.SES_income.RDS"))
null.ldi.SES_degree <- readRDS(paste0(model_fits_dir, "SES/null.ldi.SES_degree.RDS"))
null.ldi.SES_occupation <- readRDS(paste0(model_fits_dir, "SES/null.ldi.SES_occupation.RDS"))


### compare models -----------
comparison.dprime.SES_sum <- loo_compare(full.dprime.SES_sum.Gender, full.dprime.SES_sum, null.dprime.SES_sum)
comparison.dprime.SES_income <- loo_compare(full.dprime.SES_income, null.dprime.SES_income)
comparison.dprime.SES_degree <- loo_compare(full.dprime.SES_degree, null.dprime.SES_degree)
comparison.dprime.SES_occupation <- loo_compare(full.dprime.SES_occupation, null.dprime.SES_occupation)

comparison.ldi.SES_sum <- loo_compare(full.ldi.SES_sum.Gender, full.ldi.SES_sum, null.ldi.SES_sum)
comparison.ldi.SES_income <- loo_compare(full.ldi.SES_income, null.ldi.SES_income)
comparison.ldi.SES_degree <- loo_compare(full.ldi.SES_degree, null.ldi.SES_degree)
comparison.ldi.SES_occupation <- loo_compare(full.ldi.SES_occupation, null.ldi.SES_occupation)

print(comparison.dprime.SES_sum, simplify=F)
print(comparison.dprime.SES_income, simplify=F)
print(comparison.dprime.SES_degree, simplify=F)
print(comparison.dprime.SES_occupation, simplify=F)

print(comparison.ldi.SES_sum, simplify=F)
print(comparison.ldi.SES_income, simplify=F)
print(comparison.ldi.SES_degree, simplify=F)
print(comparison.ldi.SES_occupation, simplify=F)

### plot html tables -----
sjPlot::tab_model(full.dprime.SES_sum.Gender, full.dprime.SES_sum, null.dprime.SES_sum, file = paste0(model_comparisons_dir, "comparison.dprime.SES_sum.html"))
sjPlot::tab_model(full.dprime.SES_income, null.dprime.SES_income, file = paste0(model_comparisons_dir, "comparison.dprime.SES_income.html"))
sjPlot::tab_model(full.dprime.SES_degree, null.dprime.SES_degree, file = paste0(model_comparisons_dir, "comparison.dprime.SES_degree.html"))
sjPlot::tab_model(full.dprime.SES_occupation, null.dprime.SES_occupation, file = paste0(model_comparisons_dir, "comparison.dprime.SES_occupation.html"))
sjPlot::tab_model(full.ldi.SES_sum.Gender, full.ldi.SES_sum, null.ldi.SES_sum, file = paste0(model_comparisons_dir, "comparison.ldi.SES_sum.html"))
sjPlot::tab_model(full.ldi.SES_income, null.ldi.SES_income, file = paste0(model_comparisons_dir, "comparison.ldi.SES_income.html"))
sjPlot::tab_model(full.ldi.SES_degree, null.ldi.SES_degree, file = paste0(model_comparisons_dir, "comparison.ldi.SES_degree.html"))
sjPlot::tab_model(full.ldi.SES_occupation, null.ldi.SES_occupation, file = paste0(model_comparisons_dir, "comparison.ldi.SES_occupation.html"))

### plot estimates ----
sjPlot::plot_model(full.dprime.SES_sum, 
                   vline.color = "black", 
                   show.values = TRUE, 
                   value.offset = .3,
                   bpe = "mean",
                   bpe.style = "dot",
                   prob.inner = .5,
                   prob.outer = .95) + sjPlot::theme_sjplot()

ggsave("Figures/SES/dprime_SES_sum_full_model_results.png",
       plot = last_plot(),
       device = "png",
       width = 18,
       height = 10,
       units = c("cm"),
       dpi = 300,
       bg = "white") 

sjPlot::plot_model(full.ldi.SES_sum, 
                   vline.color = "black", 
                   show.values = TRUE, 
                   value.offset = .3,
                   bpe = "mean",
                   bpe.style = "dot",
                   prob.inner = .5,
                   prob.outer = .95) + sjPlot::theme_sjplot()

ggsave("Figures/SES/ldi_SES_sum_full_model_results.png",
       plot = last_plot(),
       device = "png",
       width = 18,
       height = 10,
       units = c("cm"),
       dpi = 300,
       bg = "white") 

sjPlot::plot_model(full.dprime.SES_sum.Gender, 
                   vline.color = "black", 
                   show.values = TRUE, 
                   value.offset = .3,
                   bpe = "mean",
                   bpe.style = "dot",
                   prob.inner = .5,
                   prob.outer = .95) + sjPlot::theme_sjplot()

ggsave("Figures/SES/dprime_SES_sum_Gender_full_model_results.png",
       plot = last_plot(),
       device = "png",
       width = 18,
       height = 10,
       units = c("cm"),
       dpi = 300,
       bg = "white") 

sjPlot::plot_model(full.ldi.SES_sum.Gender, 
                   vline.color = "black", 
                   show.values = TRUE, 
                   value.offset = .3,
                   bpe = "mean",
                   bpe.style = "dot",
                   prob.inner = .5,
                   prob.outer = .95) + sjPlot::theme_sjplot()

ggsave("Figures/SES/ldi_SES_sum_Gender_full_model_results.png",
       plot = last_plot(),
       device = "png",
       width = 18,
       height = 10,
       units = c("cm"),
       dpi = 300,
       bg = "white") 

### plot predictions ----
sjPlot::plot_model(full.dprime.SES_sum, type = "pred", terms = c("SES_sum"), colors = met.brewer("Lakota", n=7)[3], axis.lim=c(0,3), title="Predicted d' values depending on socio-economic status", axis.title=c("SES sum score", "d'"))


ggsave("Figures/SES/dprime_prediction_by_SES_sum.png",
       plot = last_plot(),
       device = "png",
       width = 18,
       height = 10,
       units = c("cm"),
       dpi = 300,
       bg = "white") 

sjPlot::plot_model(full.ldi.SES_sum, type = "pred", terms = c("SES_sum"), colors = met.brewer("Lakota", n=7)[3], axis.lim=c(0,3), title="Predicted LDI values depending on socio-economic status", axis.title=c("SES sum score", "LDI"))


ggsave("Figures/SES/ldi_prediction_by_SES_sum.png",
       plot = last_plot(),
       device = "png",
       width = 18,
       height = 10,
       units = c("cm"),
       dpi = 300,
       bg = "white") 

sjPlot::plot_model(full.dprime.SES_sum.Gender, type = "pred", terms = c("SES_sum", "Gender"), colors = c(met.brewer("Navajo", n=2)[2],met.brewer("Navajo", n=2)[1]), axis.lim=c(0,3), title="Predicted d' values depending on socio-economic status", axis.title=c("SES sum score", "d'"))


ggsave("Figures/SES/dprime_prediction_by_SES_sum_and_Gender.png",
       plot = last_plot(),
       device = "png",
       width = 18,
       height = 10,
       units = c("cm"),
       dpi = 300,
       bg = "white") 


sjPlot::plot_model(full.ldi.SES_sum.Gender, type = "pred", terms = c("SES_sum", "Gender"), colors = c(met.brewer("Navajo", n=2)[2],met.brewer("Navajo", n=2)[1]), axis.lim=c(0,3), title="Predicted LDI values depending on socio-economic status", axis.title=c("SES sum score", "LDI"))


ggsave("Figures/SES/ldi_prediction_by_SES_sum_and_Gender.png",
       plot = last_plot(),
       device = "png",
       width = 18,
       height = 10,
       units = c("cm"),
       dpi = 300,
       bg = "white") 
