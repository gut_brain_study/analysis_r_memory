##### author: thieleking@cbs.mpg.de

### empty environment ------
rm(list=ls())

### load packages -------
library(ggplot2)
library(MetBrewer)

### set paths ------
working_dir = "/data/pt_02020/wd/Behav/memory/"
model_fits_dir = "/data/pt_02020/wd/Behav/memory/model_fits/"
model_comparisons_dir = "/data/pt_02020/Analysis/analysis_r_memory/Model_comparisons/"

### load dataframe ---------
recognition_performance <- read.table(paste0(working_dir, "recognition_performance_clean_with_neoffi.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

discrimination_performance <- read.table(paste0(working_dir, "discrimination_performance_clean_with_neoffi.csv"), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

memory_task <- readRDS(paste0(working_dir, "memory_task_output_correctedSDT_with_curated_wanting_after_exclusion_with_anthropometrics_and_image_characteristica_and_neoffi.RDS"))

### load ANT results ------
ANT_raw <- read.table("/data/pt_02020/000_STUDY_RESULTS/ANT/ANT_raw_data_allTP.csv", stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)

for(i in 3:26){
  ANT_raw[,i] <- as.numeric(ANT_raw[,i])
}

### calculate networks -----
ANT_results <- ANT_raw[,c(1,2,27:29)]
ANT_results$alerting <- NA #### no cue - double cue
ANT_results$orienting <- NA #### central - spatial cue
ANT_results$executive.attention <- NA #### congruent - incongruent trials

for(i in 1:length(ANT_results$subject)){
  ANT_results$alerting[i] <- mean(c(ANT_raw$incong_nocue[i], ANT_raw$cong_nocue[i], ANT_raw$neut_nocue[i])) - mean(c(ANT_raw$incong_dcue[i], ANT_raw$cong_dcue[i], ANT_raw$neut_dcue[i])) 
  ANT_results$orienting[i] <- mean(c(ANT_raw$incong_ccue[i], ANT_raw$cong_ccue[i], ANT_raw$neut_ccue[i])) - mean(c(ANT_raw$incong_scue[i], ANT_raw$cong_scue[i], ANT_raw$neut_scue[i]))
  ANT_results$executive.attention[i] <- mean(c(ANT_raw$incong_scue[i], ANT_raw$incong_dcue[i], ANT_raw$incong_ccue[i], ANT_raw$incong_nocue[i])) - mean(c(ANT_raw$cong_scue[i], ANT_raw$cong_dcue[i], ANT_raw$cong_ccue[i], ANT_raw$cong_nocue[i]))
}

### export ANT results ------
write.table(ANT_results, "/data/pt_02020/000_STUDY_RESULTS/ANT/ANT_results_allTP.csv", sep="\t", row.names = F)


### add ANT values to data frames -------
recognition_performance$alerting = NA
recognition_performance$orienting = NA
recognition_performance$executive.attention = NA

discrimination_performance$alerting = NA
discrimination_performance$orienting = NA
discrimination_performance$executive.attention = NA

memory_task$alerting = NA
memory_task$orienting = NA
memory_task$executive.attention = NA


for(i in 1:length(ANT_results$subject)){
  subj = ANT_results$subject[i]
  tp = ANT_results$timepoint[i]
  recognition_performance$alerting[recognition_performance$Subject == subj & recognition_performance$Timepoint == tp] <- ANT_results$alerting[i]
  recognition_performance$orienting[recognition_performance$Subject == subj & recognition_performance$Timepoint == tp] <- ANT_results$orienting[i]
  recognition_performance$executive.attention[recognition_performance$Subject == subj & recognition_performance$Timepoint == tp] <- ANT_results$executive.attention[i]
  discrimination_performance$alerting[discrimination_performance$Subject == subj & discrimination_performance$Timepoint == tp] <- ANT_results$alerting[i]
  discrimination_performance$orienting[discrimination_performance$Subject == subj & discrimination_performance$Timepoint == tp] <- ANT_results$orienting[i]
  discrimination_performance$executive.attention[discrimination_performance$Subject == subj & discrimination_performance$Timepoint == tp] <- ANT_results$executive.attention[i]
  memory_task$alerting[memory_task$Subject == subj & memory_task$Timepoint == tp] <- ANT_results$alerting[i]
  memory_task$orienting[memory_task$Subject == subj & memory_task$Timepoint == tp] <- ANT_results$orienting[i]
  memory_task$executive.attention[memory_task$Subject == subj & memory_task$Timepoint == tp] <- ANT_results$executive.attention[i]
}

### export data frames --------
write.table(recognition_performance, paste0(working_dir, "recognition_performance_clean_with_ANT.csv"), sep="\t")

write.table(discrimination_performance, paste0(working_dir, "discrimination_performance_clean_with_ANT.csv"), sep="\t")

saveRDS(memory_task, paste0(working_dir, "memory_task_output_correctedSDT_with_curated_wanting_after_exclusion_with_anthropometrics_and_image_characteristica_and_neoffi_and_ANT.RDS"))

### visualise --------

ggplot(recognition_performance, aes(x = alerting, y=dprime)) +
  geom_point(aes(group=Subject, col=Subject, alpha=0.5)) +
  scale_color_manual(values = met.brewer("Cross",n=59)) +
  xlab("Alerting") +
  ylab("d'") +
  theme(legend.position = "none")

ggsave("Figures/ANT/dprime_alerting.png",
       plot = last_plot(),
       device = "png",
       width = 13,
       height = 8,
       units = c("cm"),
       dpi = 300,
       bg = "white")

ggplot(discrimination_performance, aes(x = alerting, y=LDI)) +
  geom_point(aes(group=Subject, col=Subject, alpha=0.5)) +
  scale_color_manual(values = met.brewer("Cross",n=59)) +
  xlab("Alerting") +
  ylab("LDI") +
  theme(legend.position = "none")

ggsave("Figures/ANT/ldi_alerting.png",
       plot = last_plot(),
       device = "png",
       width = 13,
       height = 8,
       units = c("cm"),
       dpi = 300,
       bg = "white")

ggplot(recognition_performance, aes(x = orienting, y=dprime)) +
  geom_point(aes(group=Subject, col=Subject, alpha=0.5)) +
  scale_color_manual(values = met.brewer("Cross",n=59)) +
  xlab("Orienting") +
  ylab("d'") +
  theme(legend.position = "none")

ggsave("Figures/ANT/dprime_orienting.png",
       plot = last_plot(),
       device = "png",
       width = 13,
       height = 8,
       units = c("cm"),
       dpi = 300,
       bg = "white")

ggplot(discrimination_performance, aes(x = orienting, y=LDI)) +
  geom_point(aes(group=Subject, col=Subject, alpha=0.5)) +
  scale_color_manual(values = met.brewer("Cross",n=59)) +
  xlab("Orienting") +
  ylab("LDI") +
  theme(legend.position = "none")

ggsave("Figures/ANT/ldi_orienting.png",
       plot = last_plot(),
       device = "png",
       width = 13,
       height = 8,
       units = c("cm"),
       dpi = 300,
       bg = "white")

ggplot(recognition_performance, aes(x = executive.attention, y=dprime)) +
  geom_point(aes(group=Subject, col=Subject, alpha=0.5)) +
  scale_color_manual(values = met.brewer("Cross",n=59)) +
  xlab("Executive Attention") +
  ylab("d'") +
  theme(legend.position = "none")

ggsave("Figures/ANT/dprime_executive_attention.png",
       plot = last_plot(),
       device = "png",
       width = 13,
       height = 8,
       units = c("cm"),
       dpi = 300,
       bg = "white")

ggplot(discrimination_performance, aes(x = executive.attention, y=LDI)) +
  geom_point(aes(group=Subject, col=Subject, alpha=0.5)) +
  scale_color_manual(values = met.brewer("Cross",n=59)) +
  xlab("Executive Attention") +
  ylab("LDI") +
  theme(legend.position = "none")

ggsave("Figures/ANT/ldi_executive_attention.png",
       plot = last_plot(),
       device = "png",
       width = 13,
       height = 8,
       units = c("cm"),
       dpi = 300,
       bg = "white")

