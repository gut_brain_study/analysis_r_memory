##### author: thieleking@cbs.mpg.de

### empty environment ------
rm(list=ls())

### load packages -------


### set paths ------
working_dir = "/data/pt_02020/wd/Behav/memory/"
model_fits_dir = "/data/pt_02020/wd/Behav/memory/model_fits/"
model_comparisons_dir = "/data/pt_02020/Analysis/analysis_r_memory/Model_comparisons/"


### load memory performance and models --------

memory_task <- readRDS(paste0(working_dir, "memory_task_output_correctedSDT_with_curated_wanting_after_exclusion_with_anthropometrics_and_image_characteristica.RDS"))
memory_task_liking <- readRDS(paste0(working_dir, "memory_task_output_correctedSDT_with_curated_wanting_after_exclusion_with_liking.RDS"))

### create dataframes with average wanting and response accuracy for each image  -------------------------
memory_task_images <- memory_task[,8:length(names(memory_task))]
unique_image_names <- unique(memory_task_images$Image)
unique_memory_task_images <- data.frame(Image=sort(unique_image_names))

### add image characteristics and calculate average Wanting and response accuracy for each image ------
for(imagename in unique_memory_task_images$Image){
  unique_memory_task_images$Category[unique_memory_task_images$Image == imagename] <- memory_task_images$Category[memory_task_images$Image == imagename][1]
  unique_memory_task_images$Status[unique_memory_task_images$Image == imagename] <- memory_task_images$Status[memory_task_images$Image == imagename][1]
  unique_memory_task_images$Type[unique_memory_task_images$Image == imagename] <- memory_task_images$Type[memory_task_images$Image == imagename][1]
  unique_memory_task_images$Image_Number[unique_memory_task_images$Image == imagename] <- memory_task_images$Image_Number[memory_task_images$Image == imagename][1]
  unique_memory_task_images$Style[unique_memory_task_images$Image == imagename] <- memory_task_images$Style[memory_task_images$Image == imagename][1]
  unique_memory_task_images$z_Recognizability[unique_memory_task_images$Image == imagename] <- memory_task_images$z_Recognizability[memory_task_images$Image == imagename][1]
  unique_memory_task_images$z_Arousal[unique_memory_task_images$Image == imagename] <- memory_task_images$z_Arousal[memory_task_images$Image == imagename][1]
  unique_memory_task_images$z_Valence[unique_memory_task_images$Image == imagename] <- memory_task_images$z_Valence[memory_task_images$Image == imagename][1]
  unique_memory_task_images$z_Familiarity[unique_memory_task_images$Image == imagename] <- memory_task_images$z_Familiarity[memory_task_images$Image == imagename][1]
  unique_memory_task_images$complexity_norm[unique_memory_task_images$Image == imagename] <- memory_task_images$complexity_norm[memory_task_images$Image == imagename][1]
  unique_memory_task_images$red[unique_memory_task_images$Image == imagename] <- memory_task_images$red[memory_task_images$Image == imagename][1]
  unique_memory_task_images$green[unique_memory_task_images$Image == imagename] <- memory_task_images$green[memory_task_images$Image == imagename][1]
  unique_memory_task_images$blue[unique_memory_task_images$Image == imagename] <- memory_task_images$blue[memory_task_images$Image == imagename][1]
  unique_memory_task_images$object.size[unique_memory_task_images$Image == imagename] <- memory_task_images$object.size[memory_task_images$Image == imagename][1]
  unique_memory_task_images$Wanting[unique_memory_task_images$Image == imagename] <- mean(memory_task_images$Wanting[memory_task_images$Image == imagename])
  unique_memory_task_images$Liking[unique_memory_task_images$Image == imagename] <- mean(memory_task_liking$Liking[memory_task_liking$Image == imagename], na.rm = T)
  unique_memory_task_images$response_correct[unique_memory_task_images$Image == imagename] <- mean((as.numeric(memory_task_images$response_correct[memory_task_images$Image == imagename])-1))
}

unique_memory_task_images$Category <- factor(unique_memory_task_images$Category, labels=c("NF", "F"))
unique_memory_task_images$Status <- factor(unique_memory_task_images$Status, labels=c("new", "old", "similar"))
unique_memory_task_images$Type <- factor(unique_memory_task_images$Type, labels=c("animals", "cal1", "cal2", "cal3", "cal4", "objects", "plants"))

### export dataframe with unique images ----
saveRDS(unique_memory_task_images, paste0(working_dir, "unique_memory_task_images_with_averaged_wanting_and_average_response_accuracy_after_exclusion_with_anthropometrics_and_image_characteristica.RDS"))