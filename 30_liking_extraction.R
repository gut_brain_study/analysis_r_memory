##### author: thieleking@cbs.mpg.de

#remove all dataframes in environment
rm(list=ls())

### load packages ------
library(ggplot2)
library(MetBrewer)
library(stringr)

### set paths ------
working_dir = "/data/pt_02020/wd/Behav/memory/"
model_fits_dir = "/data/pt_02020/wd/Behav/memory/model_fits/"
model_comparisons_dir = "/data/pt_02020/Analysis/analysis_r_memory/Model_comparisons/"

### load memory performance --------

memory_task <- readRDS(paste0(working_dir, "memory_task_output_correctedSDT_with_curated_wanting_after_exclusion.RDS"))


#### add liking values -----------
list_liking_txt_logfiles = list.files(path = "/data/pt_02020/000_STUDY_RESULTS/Log_Files_from_Computer/LIKING/",
                                      "*.txt", 
                                      all.files = FALSE,
                                      full.names = FALSE, recursive = FALSE,
                                      ignore.case = FALSE, include.dirs = FALSE)

list_info_txt_logfiles=list_liking_txt_logfiles[grep("info", list_liking_txt_logfiles)]

list_liking_txt_logfiles=setdiff(list_liking_txt_logfiles, list_info_txt_logfiles)

liking_txtfiles = list()
j=0
for(i in 1:length(list_liking_txt_logfiles)){
  j=j+1
  print(list_liking_txt_logfiles[i])
  liking_txtfiles[[j]] = read.table(paste0("/data/pt_02020/000_STUDY_RESULTS/Log_Files_from_Computer/LIKING/",list_liking_txt_logfiles[i]), stringsAsFactors = F, sep = "\t", skipNul = T, header=T, na.strings = "NA", fill=T, strip.white=T, blank.lines.skip=T)
}

subjects_liking=c()
for(i in 1:length(list_liking_txt_logfiles)){
  subjects_liking[i] = unlist(str_split(list_liking_txt_logfiles, "_")[[i]])[1]
}

liking <- data.frame(Subject=rep(subjects_liking, each=720), Image=NA, Category=NA,Type=NA, Image_Number=NA, Style=NA, Liking=NA)

for(i in 1:length(liking_txtfiles)){
  subj = subjects_liking[i]
  liking$Image[liking$Subject == subj] <- liking_txtfiles[[i]]$Picture
  liking$Category[liking$Subject == subj] <- liking_txtfiles[[i]]$Category
  liking$Type[liking$Subject == subj] <- liking_txtfiles[[i]]$Type
  liking$Image_Number[liking$Subject == subj] <- liking_txtfiles[[i]]$Image_No
  liking$Style[liking$Subject == subj] <- liking_txtfiles[[i]]$Style
  liking$Liking[liking$Subject == subj] <- liking_txtfiles[[i]]$Liking
}

### plot liking ratings over categories ----
liking$Liking = as.numeric(liking$Liking)
liking$Category <- factor(liking$Category, levels = c("NF", "F"))

ggplot(liking, aes(x = Liking, group=Category, fill=Category)) +
  geom_histogram(aes(y=..density..), colour="black")+
  scale_fill_manual(values=met.brewer("Lakota", n=2)) +
  facet_grid(Category ~ .)

ggsave("Figures/Liking/Liking_category_histogram.png",
       plot = last_plot(),
       device = "png",
       width = 15,
       height = 10,
       units = c("cm"),
       dpi = 300,
       bg = "white") 

### add liking values to memory_task -------
memory_task$Liking = NA

for(i in 1:length(memory_task$Subject)){
  print(i)
  subj = memory_task$Subject[i]
  image_number = memory_task$Image_Number[i]
  style = memory_task$Style[i]
  if(!identical(liking$Liking[liking$Subject == subj & liking$Image_Number == image_number & liking$Style == style], numeric(0))){
    memory_task$Liking[i] = liking$Liking[liking$Subject == subj & liking$Image_Number == image_number & liking$Style == style]
  }
  else print(paste0(c("Missing: ", subj, image_number, style)))
}

### export datafrme -------
saveRDS(memory_task, paste0(working_dir, "memory_task_output_correctedSDT_with_curated_wanting_after_exclusion_with_liking.RDS"))

### NO LIKING RATING FOR NEW AND SIMILAR IMAGES OF MEMORY TASK